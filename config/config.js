import Joi from 'joi';

require('dotenv').config({path: `.env.${process.env.NODE_ENV}`});

const envVarsSchema = Joi.object({
  PORT: Joi.number(),
  MONGODB_URL: Joi.string(),
  JWTSECRET: Joi.string(),
  EXPIRESIN: Joi.string(),
  SALT: Joi.number(),
  ADMIN_NAME: Joi.string(),
  ADMIN_EMAIL: Joi.string(),
  ADMIN_PASSWORD: Joi.string(),
  ADMIN_PHONENUMBER: Joi.string(),
  ADMIN_PROFILE_PICTURE: Joi.string(),
  API_LINK: Joi.string(),
  MAILCHIMP_EMAIL: Joi.string(),
  MAILCHIMP_API_KEY: Joi.string(),
  AWS_ACCESS_KEY: Joi.string(),
  AWS_SECRET_ACCESS_KEY: Joi.string(),
  AWS_REGION: Joi.string(),
  AWS_URL: Joi.string(),
  AWS_BUCKET: Joi.string()
})
  .unknown()
  .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  port: envVars.PORT,
  mongoURL: envVars.MONGODB_URL,
  jwtSecret: envVars.JWTSECRET,
  expiresIn: envVars.EXPIRESIN,
  salt: envVars.SALT,
  adminName: envVars.ADMIN_NAME,
  adminEmail: envVars.ADMIN_EMAIL,
  adminPassword: envVars.ADMIN_PASSWORD,
  adminPhoneNumber: envVars.ADMIN_PHONENUMBER,
  adminProfilePicture: envVars.ADMIN_PROFILE_PICTURE,
  apiLink: envVars.API_LINK,
  mailchimp_email: envVars.MAILCHIMP_EMAIL,
  mailchimp_api_key: envVars.MAILCHIMP_API_KEY,
  accessKeyId: envVars.AWS_ACCESS_KEY,
  secretAccessKey: envVars.AWS_SECRET_ACCESS_KEY,
  aws_region: envVars.AWS_REGION,
  aws_url: envVars.AWS_URL,
  aws_bucket: envVars.AWS_BUCKET,
};

export default config;