import incident from '../models/incident.model';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} incident object
 */
async function getOne(filter, select = '', populate) {
  return await incident.findOne(filter, select).populate(populate);
}

/**
 * @param {Object} body - incident object
 * @returns {Object} incidentObj
 */
async function create(body) {
  const createUser = { ...body };
  
  let incidentDetail = await incident.create(createUser);
  return { incidentDetail };
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - udpated incident object
 */
async function update(filter, body) {
  return await incident.findOneAndUpdate(filter, body, { new: true });
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - udpated incident object
 */
async function updateMany(filter, body) {
  return await incident.updateMany(filter, body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {string} select - selected fields
 * @param {number} page_size - page size
 * @param {number} page_number - page number
 * @returns {Array} - list of incidents
 */
async function list(filter, select, page_size, page_number, populate = [], sort) {
  return await incident.find(filter).sort(sort).select(select).populate(populate).skip((page_number - 1) * page_size).limit(page_size);
}

/**
 * @param {Object} body - incident object
 * @returns {Object} incidentObj
 */
async function deleteOne(body){
  return await incident.deleteOne(body);
}

module.exports = {
  getOne,
  create,
  update,
  updateMany,
  list,
  deleteOne
};