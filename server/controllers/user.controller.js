import user from '../models/user.model';
import { generatePassword } from '../helpers/utils';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} user object
 */
async function getOne(filter, select = '') {
    return await user.findOne(filter, select);
}

/**
 * @param {Object} body - user object
 * @returns {Object} userObj
 */
async function create(body) {
    let password = await generatePassword(8);
    const createUser = { ...body, password };

    let userdetail = await user.create(createUser);
    return { userdetail, password };
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - udpated user object
 */
async function update(filter, body) {
    return await user.findOneAndUpdate(filter, body, { new: true });
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} pagination - pagination
 * @param {string} select - selected fields
 * @param {Object} sort filed object for Sorting
 * @returns {Array} - list of users
 */
async function list(filter, select, page_size, page_number, sort) {
    return await user.find(filter).sort(sort).select(select).skip((page_number - 1) * page_size).limit(page_size);
}

/**
 * @param {Object} filter - filter
 * @returns {Object} user object
 */
async function deleteOne(filter) {
    return await user.deleteOne(filter);
}

module.exports = {
    getOne,
    create,
    update,
    list,
    deleteOne
};