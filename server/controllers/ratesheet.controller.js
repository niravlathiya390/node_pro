import ratesheet from '../models/ratesheet.model';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} ratesheet object
 */
async function getOne(filter, select = '') {
  return await ratesheet.findOne(filter, select);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - update ratesheet object
 */
async function update(filter, body) {
  return await ratesheet.findOneAndUpdate(filter, body);
}

/**
 * @param {Object} body - ratesheet object
 * @returns {Object} ratesheet object
 */
async function create(body) {
  return await ratesheet.create(body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {string} select - selected fields
 * @param {number} page_size - page size
 * @param {number} page_number - page number
 * @returns {Array} - list of ratesheets
 */
async function list(filter, select, populate, page_size, page_number, sort) {
  return await ratesheet.find(filter).sort(sort).select(select).populate(populate).skip((page_number - 1) * page_size).limit(page_size);
}

/**
 * @param {Object} filter - filter
 * @returns {Object} ratesheet object
 */
async function deleteOne(filter) {
  return await ratesheet.deleteOne(filter);
}

module.exports = {
  getOne,
  update,
  create,
  list,
  deleteOne
};