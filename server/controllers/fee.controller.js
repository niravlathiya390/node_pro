import fee from '../models/fee.model';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} fee object
 */
async function getOne(filter, select = '', sort = {}) {
  return await fee.findOne(filter, select).sort(sort);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - update fee object
 */
async function update(filter, body) {
  return await fee.findOneAndUpdate(filter, body);
}

/**
 * @param {Object} body - fee object
 * @returns {Object} fee object
 */
async function create(body) {
  return await fee.create(body);
}

module.exports = {
  getOne,
  update,
  create,
};