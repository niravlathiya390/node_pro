import delivery from '../models/delivery.model';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} delivery object
 */
async function getOne(filter, select = '') {
  return await delivery.findOne(filter, select);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - update delivery object
 */
async function update(filter, body) {
  return await delivery.findOneAndUpdate(filter, body);
}

/**
* @param {Object} body - delivery object
* @returns {Object} delivery object
   */
async function create(body) {
  return await delivery.create(body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {string} select - selected fields
 * @param {number} page_size - page size
 * @param {number} page_number - page number
 * @returns {Array} - list of deliveries
 */
async function list(filter, select, page_size, page_number, sort, populate = []) {
  return await delivery.find(filter).sort(sort).select(select).populate(populate).skip((page_number - 1) * page_size).limit(page_size);
}

/**
 * @param {Object} filter - filter
 * @returns {Object} delivery object
 */
async function deleteOne(filter) {
  return await delivery.deleteOne(filter);
}

module.exports = {
    getOne,
    update,
    create,
    list,
    deleteOne
};