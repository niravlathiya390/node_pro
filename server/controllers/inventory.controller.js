import inventory from '../models/inventory.model';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} vendor object
 */
async function getOne(filter, select = '') {
  return await inventory.findOne(filter, select);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - update vendor object
 */
async function update(filter, body) {
  return await inventory.findOneAndUpdate(filter, body);
}

/**
 * @param {Object} body - vendor object
 * @returns {Object} vendor object
 */
async function create(body) {
    return await inventory.create(body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {string} select - selected fields
 * @param {number} page_size - page size
 * @param {number} page_number - page number
 * @returns {Array} - list of vendors
 */
async function list(filter, select, page_size, page_number, populate = [], sort) {
  return await inventory.find(filter).sort(sort).select(select).populate(populate).skip((page_number - 1) * page_size).limit(page_size);
}

/**
 * @param {Object} filter - filter
 * @returns {Object} inventory object
 */
async function deleteOne(filter) {
  return await inventory.deleteOne(filter);
}

module.exports = {
  getOne,
  update,
  create,
  list,
  deleteOne
};