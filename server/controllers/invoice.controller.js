import invoice from '../models/invoice.model';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} invoice object
 */
async function getOne(filter, select = '') {
  return await invoice.findOne(filter, select);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - update invoice object
 */
async function update(filter, body) {
  return await invoice.findOneAndUpdate(filter, body);
}

/**
 * @param {Object} body - invoice object
 * @returns {Object} invoice object
 */
async function create(body) {
  return await invoice.create(body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {string} select - selected fields
 * @param {number} page_size - page size
 * @param {number} page_number - page number
 * @returns {Array} - list of invoices
 */
async function list(filter, select, populate, page_size, page_number, sort) {
  return await invoice.find(filter).sort(sort).select(select).populate(populate).skip((page_number - 1) * page_size).limit(page_size);
}

/**
 * @param {Object} filter - filter
 * @returns {Object} invoice object
 */
async function deleteMany(filter) {
  return await invoice.deleteMany(filter);
}

module.exports = {
  getOne,
  update,
  create,
  list,
  deleteMany
};