import client from '../models/client.model';

/**
 * @param {Object} filter - filter conditions
 * @param {string} select - selected fields
 * @returns {Object} client object
 */
async function getOne(filter, select = '', populate = []) {
  return await client.findOne(filter, select).populate(populate);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - update client object
 */
async function update(filter, body) {
  return await client.findOneAndUpdate(filter, body);
}

/**
 * @param {Object} body - client object
 * @returns {Object} client object
 */
async function create(body) {
    return await client.create(body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {string} select - selected fields
 * @param {number} page_size - page size
 * @param {number} page_number - page number
 * @returns {Array} - list of clients
 */
async function list(filter, select, populate, page_size, page_number, sort) {
  return await client.find(filter).sort(sort).select(select).populate(populate).skip((page_number - 1) * page_size).limit(page_size);
}

/**
 * @param {Object} filter - filter
 * @returns {Object} client object
 */
async function deleteOne(filter) {
  return await client.deleteOne(filter);
}

module.exports = {
    getOne,
    update,
    create,
    list,
    deleteOne
};