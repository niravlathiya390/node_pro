import note from '../models/notes.model';

/**
 * @param {Object} filter - filter conditions
 * @returns {Object} property object
 */
async function getOne(filter) {
    return await note.findOne(filter);
}

/**
 * @param {Object} body - property object
 * @returns {Object} propertyObj
 */
async function create(body) {
    return await note.create(body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {Object} body - fields to update
 * @returns {Object} - udpated property object
 */
async function update(filter, body) {
    return await note.findOneAndUpdate(filter, body);
}

/**
 * 
 * @param {Object} filter - filter
 * @param {string} select - selected fields
 * @returns {Array} - list of properties
 */
async function list(filter, select) {
    return await note.find(filter, select);
}

async function deleteOne(body){
    return await note.deleteOne(body);
  }
module.exports = {
    getOne,
    create,
    update,
    list,
    deleteOne
};