import Joi from 'joi';
import { ratesheetSize } from '../helpers/enum';

const inventoryParams = {
  //POST inventory/createInventory
  createInventory: {
    body: {
      client: Joi.string().hex().required(),
      designer: Joi.string().hex().required(),
      poReference: Joi.string().allow(''),
      description: Joi.string().required(),
      size: Joi.string().valid(Object.values(ratesheetSize)).required(),
      weight: Joi.number().required(),
      location: Joi.string().allow(''),
      freightLine: Joi.string().allow(''),
      declaredValue: Joi.number().allow(''),
      noOfBarket: Joi.number().allow(''),
      receivedDate: Joi.string().required(),
      deliveredDate: Joi.string().required(),
      copy: Joi.boolean(),
    }
  },

  //GET inventory/listInventory
  listInventory: {
    query: {
      searchString: Joi.string().allow(''),
      designer: Joi.string().hex().allow(''),
      page_size: Joi.number().required(),
      page_number: Joi.number().required()
    }
  },

  //GET inventory/getInventory
  getInventory: {
    params: {
      inventoryId: Joi.string().hex().required()
    }
  },

  //PUT inventory/updateInventory
  updateInventory: {
    body: {
      client: Joi.string().hex().required(),
      designer: Joi.string().hex().required(),
      poReference: Joi.string().allow(''),
      description: Joi.string().required(),
      size: Joi.string().valid(Object.values(ratesheetSize)).required(),
      weight: Joi.number().required(),
      location: Joi.string().allow(''),
      freightLine: Joi.string().allow(''),
      declaredValue: Joi.number().allow(''),
      noOfBarket: Joi.number().allow(''),
      receivedDate: Joi.string().required(),
      deliveredDate: Joi.string().required(),
    },
    query: {
      inventoryId: Joi.string().hex().required()
    }
  },

  //DELETE inventory/deleteInventory
  deleteInventory: {
    query: {
      inventoryId: Joi.string().hex().required()
    }
  },

  //GET inventory/getInventoryList
  getInventoryList: {
    query: {
      designer: Joi.string().hex().required(),
      client: Joi.string().hex().required()
    }
  },

  //PUT inventory/updateInventoryStatus
  updateInventoryStatus: {
    body: {
      status: Joi.boolean()
    },
    query: {
      inventoryId: Joi.string().hex().required()
    }
  }
};

export default inventoryParams;