import Joi from 'joi';
import { userTypes, userPreferdContact, reportFilter } from '../helpers/enum';

const adminParams = {
  //POST admin/createUser
  createUser: {
    body: {
      type: Joi.string().valid(Object.values(userTypes)),
      name: Joi.string().required(),
      email: Joi.string().required(),
      shipping: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      sameAsShipping: Joi.boolean(),
      billing: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      preferedContact: Joi.string().valid(Object.values(userPreferdContact)),
      billingOptions: {
        receivingFees: Joi.boolean(),
        storageFees: Joi.boolean(),
        detailOnInvoice: Joi.boolean(),
      },
    },
  },

  //GET admin/listUsers
  listUsers: {
    query: {
      searchString: Joi.string().allow(''),
      type: Joi.string().valid(Object.values(userTypes)).allow(''),
      page_size: Joi.number().required(),
      page_number: Joi.number().required(),
    },
  },

  //GET admin/getUser
  getUser: {
    params: {
      userId: Joi.string().hex().required(),
    },
  },

  //DELETE admin/deleteUser
  deleteUser: {
    query: {
      userId: Joi.string().hex().required(),
    },
  },

  //PUT admin/updateUser
  updateUser: {
    body: {
      type: Joi.string().valid(Object.values(userTypes)),
      name: Joi.string().required(),
      email: Joi.string().required(),
      shipping: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      sameAsShipping: Joi.boolean(),
      billing: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      preferedContact: Joi.string().valid(Object.values(userPreferdContact)),
      billingOptions: {
        receivingFees: Joi.boolean(),
        storageFees: Joi.boolean(),
        detailOnInvoice: Joi.boolean(),
      },
    },
    query: {
      userId: Joi.string().hex().required(),
    },
  },

  //POST admin/addNote
  addNote: {
    body: {
      note: Joi.string(),
      file: Joi.string().allow(''),
      userId: Joi.string().hex(),
      incidentId: Joi.string().hex(),
    },
  },

  //PUT admin/updateNote
  updateNote: {
    body: {
      note: Joi.string(),
      file: Joi.string().allow(''),
    },
    query: {
      noteId: Joi.string().hex().required(),
    },
  },

  //DELETE admin/deleteNote
  deleteNote: {
    query: {
      noteId: Joi.string().required(),
    },
  },

  //DELETE admin/deleteFile
  deleteFile: {
    body: {
      url: Joi.string().required(),
    },
  },

  //GET admin/listClients
  listClients: {
    query: {
      userId: Joi.string().hex().required(),
    },
  },

  //POST admin/generateReport
  generateReport: {
    query: {
      type: Joi.string().valid(Object.values(reportFilter)).required(),
      startOn: Joi.string(),
      endOn: Joi.string(),
      designer: Joi.string(),
      client: Joi.string(),
      sortBy: Joi.string()
    },
  },

  //GET admin/clientList
  clientList: {
    query: {
      designerId: Joi.string().hex().required()
    }
  },

  //PUT admin/userUpdate
  userUpdate: {
    body: {
      name: Joi.string(),
      email: Joi.string(),
      phoneNumber: Joi.string(),
      profilePicture: Joi.string().allow(''),
      sortBy: Joi.string(),
    },
  },
};

export default adminParams;