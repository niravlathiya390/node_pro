import Joi from 'joi';

const invoiceParams = {
  //GET invoice/listInvoices
  listInvoices: {
    query: {
      searchString: Joi.string().allow(''),
      designer: Joi.string().hex().allow(''),
      startDate: Joi.date().allow(''),
      endDate: Joi.date().allow(''),
      page_size: Joi.number().required(),
      page_number: Joi.number().required()
    }
  },

  //GET invoice/getInvoice
  getInvoice: {
    params: {
      invoiceId: Joi.string().hex().required()
    }
  },  

  //DELETE invoice/deleteInvoice
  deleteInvoice: {
    query: {
      invoices: Joi.array().items(Joi.string().hex())
    }
  },

  //PUT invoice/exportInvoices
  exportInvoices: {
    query: {
      invoices: Joi.array().items(Joi.string().hex())
    }
  }
};

export default invoiceParams;