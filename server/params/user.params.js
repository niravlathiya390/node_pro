import Joi from 'joi';

const userParams = {
  //POST user/changePassword
  changePassword: {
    body: {
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().required(),
    }
  },

  //PUT user/updateUser
  updateUser: {
    body: {
      name: Joi.string(),
      email: Joi.string(),
      business: Joi.string()
    }
  },

  //GET user/listIncidents
  listIncidents: {
    query: {
      searchString: Joi.string().allow(''),
    }
  },
}

export default userParams;