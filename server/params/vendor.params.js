import Joi from 'joi';

const vendorParams = {
  //POST vendor/createVendor
  createVendor: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      phoneNumber: Joi.string().required(),
      name: Joi.string().required(),
      address: Joi.string().required()
    }
  },

  //GET vendor/listVendors
  listVendors: {
    query: {
      searchString: Joi.string().allow('').required(),
      page_number: Joi.number().required(),
      page_size: Joi.number().required(),
    }
  },

  //GET vendor/getVendor
  getVendor: {
    params: {
      vendorId: Joi.string().hex().required()
    }
  },

  //DELETE vendor/deleteVendors
  deleteVendor: {
    params: {
      vendorId: Joi.string().hex().required()
    }
  },

  //PUT vendor/updateVendor
  updateVendor: {
    query: {
      vendorId: Joi.string().hex().required()
    },
    body: {
      email: Joi.string().required(),
      phoneNumber: Joi.string().required(),
      name: Joi.string().required(),
      address: Joi.string().required(),
      password: Joi.string(),
    }
  },
};

export default vendorParams;