import Joi from 'joi';

const feeParams = {
  //POST fee/createFee
  createFee: {
    body: {
      dailyRate: Joi.number().required(),
      transitRate: Joi.number().required(),
      pullFee: Joi.number().required(),
      pickupFee: Joi.number().required(),
      trashFee: Joi.number().required(),
      barketFee: Joi.number().required(),
      laborRates: Joi.array().items({
        designerRate: Joi.number(),
        customerRate: Joi.number(),
        surroundingsRate: Joi.number(),
      }),
    }
  },

  //GET fee/getFee
  getFee: {
    query: {
      feeId: Joi.string().hex().required()
    }
  },

  //PUT fee/updateFee
  updateFee: {
    query: {
      feeId: Joi.string().hex().required()
    },
    body: {
      dailyRate: Joi.number().required(),
      transitRate: Joi.number().required(),
      pullFee: Joi.number().required(),
      pickupFee: Joi.number().required(),
      trashFee: Joi.number().required(),
      barketFee: Joi.number().required(),
      laborRates: Joi.array().items({
        designerRate: Joi.number(),
        customerRate: Joi.number(),
        surroundingsRate: Joi.number(),
      }),
    }
  },
};

export default feeParams;