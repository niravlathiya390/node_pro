import Joi from 'joi';
import { ratesheetSize } from '../helpers/enum';

const ratesheetParams = {
  //POST ratesheet/createRatesheet
  createRatesheet: {
    body: {
      designer: Joi.string().hex(),
      client: Joi.string().hex(),
      effectiveOn: Joi.date(),
      ratesheet: Joi.array().items({
        size: Joi.string().valid(Object.values(ratesheetSize)),
        receivingFee: Joi.number(),
        receivingFeeDesc: Joi.boolean(),
        storageFee: Joi.number(),
        storageFeeDesc: Joi.boolean()
      }),
    }
  },

  //GET ratesheet/listRatesheets
  listRatesheets: {
    query: {
      searchString: Joi.string().allow(''),
      page_size: Joi.number().required(),
      page_number: Joi.number().required()
      }
    },

  //GET ratesheet/getRatesheet
  getRatesheet: {
    params: {
      ratesheetId: Joi.string().hex().required()
    }
  },

  //PUT ratesheet/updateRatesheet
  updateRatesheet: {
    body: {
      designer: Joi.string().hex(),
      client: Joi.string().hex(),
      effectiveOn: Joi.date(),
      ratesheet: Joi.array().items({
        size: Joi.string().valid(Object.values(ratesheetSize)),
        receivingFee: Joi.number(),
        receivingFeeDesc: Joi.boolean(),
        storageFee: Joi.number(),
        storageFeeDesc: Joi.boolean()
      }),
    },
    query: {
      ratesheetId: Joi.string().hex().required()
    }
  },

  //DELETE ratesheet/deleteRatesheet
  deleteRatesheet: {
    query: {
      ratesheetId: Joi.string().hex().required()
    }
  },

  //PUT ratesheet/updateRatesheetStatus
  updateRatesheetStatus: {
    body: {
      ultraService: Joi.boolean(),
      retail: Joi.boolean(),
    },
    query: {
      ratesheetId: Joi.string().hex().required()
    }
  }
};

export default ratesheetParams;