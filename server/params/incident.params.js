import Joi from 'joi';
import { incidentDamaged, userTypes, estimationStatus } from '../helpers/enum';

const incidentParams = {
  addIncident: {
    body: {
      barCodeId: Joi.string().required(),
      barCodeItemId: Joi.string().required(),
      wsiNumber: Joi.string().required(),
      description: Joi.string(),
      itemPicture: Joi.array().items( 
        Joi.string()
      ),
      damaged: Joi.string().valid(Object.values(incidentDamaged)).required(),
      notes: Joi.string()
    },
  },

  //GET incident/listIncidents
  listIncidents: {
    query: {
      searchString: Joi.string().allow(''),
      employeeFilter: Joi.string().hex().allow(''),
      page_size: Joi.number().required(),
      page_number: Joi.number().required()
    }
  },

  //GET incident/getIncident
  getIncident: {
    params: {
      incidentId: Joi.string().hex().required()
    }
  },

  //PUT incident/updateIncident
  updateIncident: {
    query: {
      incidentId: Joi.string().hex().required()
    },
    body: {
      barCodeId: Joi.string().required(),
      barCodeItemId: Joi.string().required(),
      wsiNumber: Joi.string().required(),
      description: Joi.string(),
      itemPicture: Joi.array().items( 
        Joi.string()
      ),
      damaged: Joi.string().valid(Object.values(incidentDamaged)).required(),
      employee: Joi.string().hex()
    }
  },

  //DELETE incident/deleteIncident
  deleteIncident: {
    query: {
      incidentId: Joi.string().hex().required()
    }
  },

  //PUT incident/createEstimation
  createEstimation: {
    body: {
      description: Joi.string(),
      total: Joi.number(),
      file: Joi.string().allow('')
    }
  },

  //PUT incident/assignToVendor
  assignToVendor: {
    query: {
      incidentId: Joi.string().hex().required(),
      vendorId: Joi.string().hex().required()
    }
  },
  
  //PUT incident/updateEstimationStatus
  updateEstimationStatus: {
    query: {
      incidentId: Joi.string().hex().required(),
      estimationStatus: Joi.string().valid(Object.values(estimationStatus)).required()
    }
  }
};

export default incidentParams