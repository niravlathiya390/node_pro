import Joi from 'joi';

const authParams = {
  //POST auth/login
  login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
    }
  },

  //POST auth/forgetPassword
  forgotPassword: {
    body: {
      email: Joi.string()
    }
  },

  //POST auth/resetUserPassword
  resetUserPassword: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
    }
  },
};

export default authParams;