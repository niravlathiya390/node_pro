import Joi from 'joi';

const clientParams = {
  //POST client/createClient
  createClient: {
    body: {
      client: Joi.string().required(),
      designer: Joi.string().hex().required(),
      email: Joi.string().required(),
      primaryAddress: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      secondaryAddress: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      sameAsPrimaryAddress: Joi.boolean(),
      acceptDVP: Joi.boolean(),
      options: {
        active: Joi.boolean(),
        displayNotes: Joi.boolean(),
        ultraService: Joi.boolean(),
        retail: Joi.boolean(),
      }
    }
  },

  //GET client/listClients
  listClients: {
    query: {
      searchString: Joi.string().allow(''),
      page_size: Joi.number().required(),
      page_number: Joi.number().required()
    }
  },

  //GET client/getClient
  getClient: {
    params: {
      clientId: Joi.string().hex().required()
    }
  },

  //PUT client/updateClient
  updateClient: {
    body: {
      client: Joi.string().required(),
      designer: Joi.string().hex().required(),
      email: Joi.string().required(),
      primaryAddress: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      sameAsPrimaryAddress: Joi.boolean(),
      secondaryAddress: {
        zipcode: Joi.number(),
        city: Joi.string(),
        state: Joi.string(),
        address: Joi.string(),
        business: Joi.string(),
        cel: Joi.string(),
        fax: Joi.string(),
        other: Joi.string(),
      },
      acceptDVP: Joi.boolean(),
      options: {
        active: Joi.boolean(),
        displayNotes: Joi.boolean(),
        ultraService: Joi.boolean(),
        retail: Joi.boolean(),
      }
    },
    query: {
      clientId: Joi.string().hex().required()
    }
  },

  //DELETE client/deleteClient
  deleteClient: {
    query: {
      clientId: Joi.string().hex().required()
    }
  },
};

export default clientParams;