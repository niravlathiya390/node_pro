import Joi from 'joi';

const deliveryParams = {
  //POST delivery/createDelivery
  createDelivery: {
    body: {
      designer: Joi.string().hex().required(),
      client: Joi.string().hex().required(),
      item: Joi.array().items().required(),
      deliveredOn: Joi.date().required(),
      doNotBill: Joi.boolean().required(),
      clientPickUp: Joi.boolean().required(),
      notes: Joi.boolean().required(),
      total: Joi.number().required(),
      invoicedOn: Joi.date(),
      closedOn: Joi.date(),
      options: {
        cod: Joi.boolean(),
        billed: Joi.boolean(),
        prepay: Joi.boolean()
      },
      designerNotes: Joi.boolean(),
    }
  },

  //GET delivery/listDeliveries
  listDeliveries: {
    query: {
      searchString: Joi.string().allow(''),
      designer: Joi.string().hex().allow(''),
      page_size: Joi.number().required(),
      page_number: Joi.number().required()
    }
  },

  //GET delivery/getDelivery
  getDelivery: {
    params: {
      deliveryId: Joi.string().hex().required()
    }
  },

  //PUT delivery/updateDelivery
  updateDelivery: {
    body: {
      designer: Joi.string().hex(),
      client: Joi.string().hex(),
      item: Joi.array().items(),
      deliveredOn: Joi.date(),
      doNotBill: Joi.boolean(),
      clientPickUp: Joi.boolean(),
      notes: Joi.boolean(),
      total: Joi.number(),
      invoicedOn: Joi.date(),
      closedOn: Joi.date(),
      options: {
        cod: Joi.boolean(),
        billed: Joi.boolean(),
        prepay: Joi.boolean()
      },
      designerNotes: Joi.boolean(),
    },
    query: {
      deliveryId: Joi.string().hex().required()
    }
  },

  //DELETE delivery/deleteDelivery
  deleteDelivery: {
    query: {
      deliveryId: Joi.string().hex().required()
    }
  },
};

export default deliveryParams;