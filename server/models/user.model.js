import mongoose, { Schema } from 'mongoose';
import bcrypt from "bcrypt";
import { salt } from '../../bin/www';
import { roles } from '../helpers/roles';
import { userTypes, userPreferdContact } from '../helpers/enum';

const shippingSchema = new Schema(
  {
    zipcode: {
      type: Number
    },
    city: {
      type: String
    },
    state: {
      type: String
    },
    address: {
      type: String
    },
    business: {
      type: String
    },
    cel: {
      type: String
    },
    fax: {
      type: String
    },
    other: {
      type: String
    }
  });

const userSchema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      required: true,
      lowercase: true,
      trim: true
    },
    address: {
      type: String
    },
    password: {
      type: String,
      required: true
    },
    phoneNumber: {
      type: String
    },
    role: {
      type: String,
      enum: roles
    },
    type: {
      type: String,
      enum: Object.values(userTypes)
    },
    shipping: shippingSchema,
    sameAsShipping: {
      type: Boolean
    },
    billing: shippingSchema,
    preferedContact: {
      type: String,
      enum: Object.values(userPreferdContact)
    },
    billingOptions: {
      receivingFees: {
        type: Boolean
      },
      storageFees: {
        type: Boolean
      },
      detailOnInvoice: {
        type: Boolean
      }
    },
    notes: [ 
      {
        type: mongoose.Types.ObjectId,
        ref: 'notes',
        default: undefined
      }
     ],
    activeSessions: [{
      type: String,
      default: [],
    }],
    profilePicture: {
      type: String,
      default: ''
    },
  }, { collection: 'user', timestamps: true }
)

userSchema.pre("save", async function (next) {
  const user = this;
  if(user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, salt);
  }
  next();
});

userSchema.methods.matchPassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

const user = mongoose.model('user', userSchema)
module.exports = user