import mongoose, { Schema } from 'mongoose';

const optionsSchema = new Schema(
  {
    cod: {
      type: Boolean
    },
    billed: {
      type: Boolean
    },
    prepay: {
      type: Boolean
    }
  });

const deliverySchema = new Schema(
  {
    ID: {
      type: String,
    },
    invoiceId: {
      type: mongoose.Types.ObjectId,
      ref: 'invoice'
    },
    designer: {
      type: mongoose.Types.ObjectId,
      ref: 'user'
    },
    client: {
      type: mongoose.Types.ObjectId,
      ref: 'client',
    },
    item: [{
      type: mongoose.Types.ObjectId,
      ref: 'inventory',
    }],
    deliveredOn: {
      type: Date
    },
    doNotBill: {
      type: Boolean
    },
    clientPickUp: {
      type: Boolean
    },
    notes: {
      type: Boolean
    },
    total: {
      type: Number
    },
    invoicedOn: {
      type: Date
    },
    closedOn: {
      type: Date
    },
    options: optionsSchema,
    designerNotes: {
      type: Boolean
    }
  }, { collection: 'delivery', timestamps: true }
)

const delivery = mongoose.model('delivery', deliverySchema);
module.exports = delivery;