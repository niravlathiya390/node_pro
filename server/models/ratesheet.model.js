import mongoose, { Schema } from 'mongoose';
import { ratesheetSize } from '../helpers/enum';

const rateSchema = new Schema(
  {
    size: {
      type: String,
      enum: Object.values(ratesheetSize),
    },
    receivingFee: {
      type: Number
    },
    receivingFeeDesc: {
      type: Boolean
    },
    storageFee: {
      type: Number
    },
    storageFeeDesc: {
      type: Boolean
    }
  }
)

const ratesheetSchema = new Schema(
  {
    ID: {
      type: String,
    },
    designer: {
      type: mongoose.Types.ObjectId,
      ref: 'user'
    },
    client: {
      type: mongoose.Types.ObjectId,
      ref: 'client',
    },
    effectiveOn: {
      type: Date
    },
    ratesheet: [rateSchema],
    ultraService: {
      type: Boolean,
      default: false
    },
    retail: {
      type: Boolean,
      default: false
    }
  }, { collection: 'ratesheet', timestamps: true }
)

const ratesheet = mongoose.model('ratesheet', ratesheetSchema);
module.exports = ratesheet;