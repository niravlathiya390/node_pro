import mongoose, { Schema } from 'mongoose';

const laborRatesSchema = new Schema(
  {
    designerRate: {
      type: Number,
    },
    customerRate: {
      type: Number,
    },
    surroundingsRate: {
      type: Number,
    }
});

const feeSchema = new Schema(
  {
    dailyRate: {
      type: Number,
    },
    transitRate: {
      type: Number,
    },
    pullFee: {
      type: Number,
    },
    pickupFee: {
      type: Number,
    },
    trashFee: {
      type: Number,
    },
    barketFee: {
      type: Number
    },
    laborRates: [laborRatesSchema]
  }, { collection: 'fee', timestamps: true }
)

const fee = mongoose.model('fee', feeSchema);
module.exports = fee;