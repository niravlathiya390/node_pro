import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import { salt } from '../../bin/www';

const addressSchema = new Schema(
  {
    zipcode: {
    type: Number
    },
    city: {
    type: String
    },
    state: {
    type: String
    },
    address: {
    type: String
    },
    business: {
    type: String
    },
    cel: {
    type: String
    },
    fax: {
    type: String
    },
    other: {
    type: String
    }
  });

const optionsSchema = new Schema(
  {
    active: {
      type: Boolean
    },
    displayNotes: {
      type: Boolean
    },
    ultraService: {
      type: Boolean
    },
    retail: {
      type: Boolean
    }
  });

const clientSchema = new Schema(
  {
    ID: {
      type: String,
    },
    client: {
      type: String,
    },
    designer: {
      type: mongoose.Types.ObjectId,
      ref: 'user'
    },
    email: {
      type: String,
      required: true,
      lowercase: true,
      trim: true
    },
    primaryAddress: addressSchema,
    sameAsPrimaryAddress: {
      type: Boolean
    },
    secondaryAddress: addressSchema,
    acceptDVP: {
      type: Boolean
    },
    options: optionsSchema
  }, { collection: 'client', timestamps: true }
)

clientSchema.pre('save', async function (next) {
  const client = this;
  if(client.isModified('password')) {
    client.password = await bcrypt.hash(client.password, salt);
  }
  next();
});

clientSchema.methods.matchPassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

const client = mongoose.model('client', clientSchema)
module.exports = client