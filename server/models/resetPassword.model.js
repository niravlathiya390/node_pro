import mongoose, { Schema } from 'mongoose';

const resetPassword = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "user",
  },
  token: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    expires: '1h'// this is the expiry time in seconds
  },
});
module.exports = mongoose.model("resetPassword", resetPassword);