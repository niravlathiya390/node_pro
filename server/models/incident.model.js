import mongoose, { Schema } from 'mongoose';
import { incidentDamaged, incidentStatus, estimationStatus } from '../helpers/enum';

const estimationSchema = new Schema({
  description: {
    type: String,
  },
  total: {
    type: Number,
  },
  file: {
    type: String,
  },
  estimationStatus: {
    type: String,
    enum: Object.values(estimationStatus),
    default: estimationStatus.pending
  },
  estimationDate: {
    type: Date
  }
});

const incidentSchema = new Schema(
  {
    barCodeId: {
      type: String,
      required: true,
    },
    barCodeItemId: {
      type: String,
      required: true,
    },
    wsiNumber: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    itemPicture: [
      {
        type: String,
      },
    ],
    damaged: {
      type: String,
      enum: Object.values(incidentDamaged),
    },
    notes: [
      {
        type: mongoose.Types.ObjectId,
        ref: 'notes',
        default: undefined,
      },
    ],
    employee: {
      type: mongoose.Types.ObjectId,
      ref: 'user',
    },
    estimation: estimationSchema,
    status: {
      type: String,
      enum: Object.values(incidentStatus),
      default: incidentStatus.pendingAssigned,
    },
    designer: {
      type: mongoose.Types.ObjectId,
      ref: 'user',
    },
    vendor: {
      type: mongoose.Types.ObjectId,
      ref: 'user',
    },
  },
  { collection: 'incident', timestamps: true }
);

incidentSchema.statics = {
  /**
   * List user in assending order of 'createdAt' timestamp.
   */

  list(filter, { skip = 0, limit = 0 } = {}) {
    return this.find(filter)
      .populate([{ path: 'employee', select: '_id name email type' }])
      .sort({ createdAt: 1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  },
};

const incident = mongoose.model('incident', incidentSchema);
module.exports = incident;