import mongoose, { Schema } from 'mongoose';

const notesSchema = new Schema({
    note: {
      type: String
    },
    file: {
      type: String
    },
    userId: {
      type: mongoose.Types.ObjectId,
      ref: 'user'
    },
    incidentId: {
      type: mongoose.Types.ObjectId,
      ref: 'incident'
    }
  }, { collection: 'notes', timestamps: true }
  );

const note = mongoose.model('notes', notesSchema);
module.exports = note;