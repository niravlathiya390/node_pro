import mongoose, { Schema } from 'mongoose';

const invoiceSchema = new Schema(
  {
    ID: {
      type: String,
    },
    designer: {
      type: mongoose.Types.ObjectId,
      ref: 'user'
    },
    client: {
      type: mongoose.Types.ObjectId,
      ref: 'client',
    },
    invoiceDate: {
      type: Date
    },
    deliveryFee: {
      type: String,
    },
    DVP_Fee: {
      type: String,
    },
    pickUp: {
      type: Boolean,
    },
    method: {
      type: String,
    }
  }, { collection: 'invoice', timestamps: true }
)

const invoice = mongoose.model('invoice', invoiceSchema);
module.exports = invoice;