import mongoose, { Schema } from 'mongoose';
import { ratesheetSize } from '../helpers/enum';

const inventorySchema = new Schema(
  {
    ID: {
      type: String,
    },
    client: {
      type: mongoose.Types.ObjectId,
      ref: 'client',
    },
    designer: {
      type: mongoose.Types.ObjectId,
      ref: 'user'
    },
    poReference: {
      type: String,
    },
    description: {
      type: String,
    },
    size: {
      type: String,
      enum: Object.values(ratesheetSize),
    },
    weight: {
      type: Number,
    },
    location: {
      type: String,
    },
    freightLine: {
      type: String,
    },
    declaredValue: {
      type: Number,
      default: 0
    },
    noOfBarket: {
      type: Number,
    },
    receivedDate: {
      type: Date,
    },
    deliveredDate: {
      type: Date
    },
    barcode: {
      type: String
    },
    status: {
      type: Boolean,
      default: true
    }
  }, { collection: 'inventory', timestamps: true }
)

const inventory = mongoose.model('inventory', inventorySchema);
module.exports = inventory;