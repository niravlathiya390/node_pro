import express from 'express';
import validate from 'express-validation';
import admin from '../beans/admin';
import adminParams from '../params/admin.params';
import { checkPermissions } from '../helpers/middleware';
import S3UploadService from '../services/file.service';
import multer from 'multer';
let storage = multer.memoryStorage();
let multipleUpload = multer({ storage: storage });

const router = express.Router();

router
  .route('/createUser')
  /** check user permissions */
  .all(checkPermissions('create_user'))
  /** POST admin/createUser - create user */
  .post(validate(adminParams.createUser), admin.createUser);

router
  .route('/listUsers')
  /** check user permissions */
  .all(checkPermissions('list_users'))
  /** GET admin/listUser - list users */
  .get(validate(adminParams.listUsers), admin.listUsers);

router
  .route('/getUser/:userId')
  /** check user permissions */
  .all(checkPermissions('get_user'))
  /** GET admin/getUser - get user by id */
  .get(validate(adminParams.getUser), admin.getUser);

router
  .route('/deleteUser')
  /** check user permissions */
  .all(checkPermissions('delete_user'))
  /** DELETE admin/deleteUser - delete user */
  .delete(validate(adminParams.deleteUser), admin.deleteUser);

router
  .route('/updateUser')
  /** check user permissions */
  .all(checkPermissions('update_user'))
  /** PUT admin/updateUser - update user */
  .put(validate(adminParams.updateUser), admin.updateUser);

router
  .route('/addnote')
  /** check user permissions */
  .all(checkPermissions('add_note'))
  /** POST admin/addnote - add note */
  .post(validate(adminParams.addNote), admin.addNote);

router
  .route('/updateNote')
  /** check user permissions */
  .all(checkPermissions('update_note'))
  /** PUT admin/updateNote - update note */
  .put(validate(adminParams.updateNote), admin.updateNote);

router
  .route('/deleteNote')
  /** check user permissions */
  .all(checkPermissions('delete_note'))
  /** DELETE admin/deleteNote - delete note */
  .delete(validate(adminParams.deleteNote), admin.deleteNote);

router
  .route('/uploadFile')
  /** POST admin/uploadFile - upload file buy user */
  .post(multipleUpload.array('files'), S3UploadService.fileUpload, admin.uploadFile);

router
  .route('/deleteFile')
  /** DELETE admin/deleteFile - delete file from AWS */
  .delete(validate(adminParams.deleteFile), admin.deleteFile);

router
  .route('/listClients')
  /** check user permissions */
  .all(checkPermissions('list_clients'))
  /** GET admin/listClients - list clients by user */
  .get(validate(adminParams.listClients), admin.listClients);

router
  .route('/listDesigners')
  /** check user permissions */
  .all(checkPermissions('list_designers'))
  /** GET admin/listDesigners - list designers */
  .get(admin.listDesigners);

router
  .route('/listEmployees')
  /** check user permissions */
  .all(checkPermissions('list_employees'))
  /** GET admin/listEmployees - list employees */
  .get(admin.listEmployees);

router
  .route('/clientList')
  /** check user permissions */
  .all(checkPermissions('client_list'))
  /** GET admin/clientList - client list */
  .get(validate(adminParams.clientList), admin.clientList);

router
  .route('/getUserById')
  /** check user permissions */
  .all(checkPermissions('get_user_by_id'))
  /** GET admin/getUserById - get user by id */
  .get(admin.getUserById);

router
  .route('/userUpdate')
  /** check user permissions */
  .all(checkPermissions('user_update'))
  /** PUT admin/userUpdate - user update */
  .put(multipleUpload.array('files'), S3UploadService.fileUpload, validate(adminParams.userUpdate), admin.userUpdate);

router
  .route('/generateReport')
  /** check user permissions */
  .all(checkPermissions('generate_report'))
  /** POST admin/generateReport - generate report by admin */
  .post(validate(adminParams.generateReport), admin.generateReport);

module.exports = router;