import express from 'express';
import validate from 'express-validation';
import authParams from '../params/auth.paramas';
import auth from '../beans/auth';
const router = express.Router();

router
  .route('/login')
  /** POST auth/login - user Login */
  .post(validate(authParams.login), auth.login);

router
  .route('/appLogin')
  /** POST auth/appLogin - app login */
  .post(validate(authParams.login), auth.appLogin);

router
  .route('/forgotPassword')
  /** POST auth/forgotPassword - password forgot by user */
  .post(validate(authParams.forgotPassword), auth.forgotPassword);

router
  .route('/renderForgetPasswordForm')
  /** GET auth/renderForgetPasswordForm - render Forget Password Form to reset password */
  .get(auth.renderForgetPasswordForm);

router
  .route('/resetUserPassword')
  /** POST auth/resetPassword - reset password by user */
  .post(validate(authParams.resetUserPassword), auth.resetUserPassword);

module.exports = router;