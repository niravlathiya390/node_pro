import express from 'express';
import validate from 'express-validation';
import user from '../beans/user';
import userParams from '../params/user.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/changePassword')
  /** check user permissions */
  .all(checkPermissions('change_password'))
  /** POST user/changePassword - reset password by user */
  .post(validate(userParams.changePassword), user.changePassword);

router
  .route('/getUser')
  /** check user permissions */
  .all(checkPermissions('get_user_app'))
  /** GET user/getUser - get user */
  .get(user.getUser);

router
  .route('/updateUser')
  /** check user permissions */
  .all(checkPermissions('update_user_app'))
  /** PUT user/updateUser - update user */
  .put(validate(userParams.updateUser), user.updateUser);

router
  .route('/listIncidents')
  /** check user permissions */
  .all(checkPermissions('list_incidents_app'))
  /** GET user/listIncidents - list incidents */
  .get(validate(userParams.listIncidents), user.listIncidents);

module.exports = router;