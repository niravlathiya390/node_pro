import express from 'express';
import validate from 'express-validation';
import invoice from '../beans/invoice';
import invoiceParams from '../params/invoice.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/listInvoices')
  /** check user permissions */
  .all(checkPermissions('list_invoice'))
  /** GET invoice/listInvoices - list invoices by permitted users */
  .get(validate(invoiceParams.listInvoices), invoice.listInvoices);

router
  .route('/getInvoice/:invoiceId')
  /** check user permissions */
  .all(checkPermissions('get_invoice'))
  /** GET invoice/getInvoice - get invoice by id */
  .get(validate(invoiceParams.getInvoice), invoice.getInvoice);

router
  .route('/deleteInvoice')
  /** check user permissions */
  .all(checkPermissions('delete_invoice'))
  /** DELETE invoice/deleteInvoice - delete invoice */
  .delete(validate(invoiceParams.deleteInvoice), invoice.deleteInvoice);

router
  .route('/exportInvoices')
  /** check user permissions */
  .all(checkPermissions('export_invoices'))
  /** PUT invoice/exportInvoices - export invoice */
  .put(validate(invoiceParams.exportInvoices), invoice.exportInvoices);

module.exports = router;