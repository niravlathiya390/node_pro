import express from 'express';
import validate from 'express-validation';
import incidentParams from '../params/incident.params';
import incident from "../beans/incident";
import { checkPermissions } from '../helpers/middleware';
const router = express.Router();

router
  .route('/addIncident')
  /** check user permissions */
  .all(checkPermissions('add_incident'))
  /** POST incident/addIncident - add incident */
  .post(validate(incidentParams.addIncident), incident.addIncident);

router
  .route('/listIncidents')
  /** check user permissions */
  .all(checkPermissions('list_incidents'))
  /** GET incident/listIncidents - list incidents */
  .get(validate(incidentParams.listIncidents), incident.listIncidents);

router
  .route('/getIncident/:incidentId')
  /** check user permissions */
  .all(checkPermissions('get_incident'))
  /** GET incident/getIncident - get incident by id */
  .get(validate(incidentParams.getIncident), incident.getIncident);

router
  .route('/updateIncident')
  /** check user permissions */
  .all(checkPermissions('update_incident'))
  /** PUT incident/updateIncident - update incident */
  .put(validate(incidentParams.updateIncident), incident.updateIncident);

router
  .route('/deleteIncident')
  /** check user permissions */
  .all(checkPermissions('delete_incident'))
  /** DELETE incident/deleteIncident - delete incident */
  .delete(validate(incidentParams.deleteIncident), incident.deleteIncident);

router
  .route('/createEstimation')
  /** check user permissions */
  .all(checkPermissions('create_estimation'))
  /** PUT incident/createEstimation - create estimation by permitted users */
  .put(validate(incidentParams.createEstimation), incident.createEstimation);

router
  .route('/assignToVendor')
  /** check user permissions */
  .all(checkPermissions('assign_to_vendor'))
  /** PUT incident/assignToVendor - assign vendor in incident by permitted users */
  .put(validate(incidentParams.assignToVendor), incident.assignToVendor);

router
  .route('/updateEstimation')
  /** check user permissions */
  .all(checkPermissions('update_estimation'))
  /** PUT incident/updateEstimation - update estimation status in incident by permitted users */
  .put(validate(incidentParams.updateEstimationStatus), incident.updateEstimationStatus);

module.exports = router