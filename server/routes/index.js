import express from 'express';
import authRoutes from './auth.route';
import adminRoutes from './admin.route';
import vendorRoutes from './vendor.route';
import incidentRoutes from './incident.route';
import feeRoutes from './fee.route';
import clientRoutes from './client.route';
import inventoryRoutes from './inventory.route';
import deliveryRoutes from './delivery.route';
import invoiceRoutes from './invoice.route';
import ratesheetRoutes from './ratesheet.route';
import userRoutes from './user.route';
import { authorize } from '../beans/auth';

const router = express.Router();

/*list APIs */
router.use('/auth', authRoutes);

router.use(authorize)

/* authorized routes APIs */
router.use('/admin', adminRoutes);
router.use('/vendor', vendorRoutes);
router.use('/incident', incidentRoutes)
router.use('/fee', feeRoutes);
router.use('/client', clientRoutes);
router.use('/inventory', inventoryRoutes);
router.use('/delivery', deliveryRoutes);
router.use('/invoice', invoiceRoutes);
router.use('/ratesheet', ratesheetRoutes);
router.use('/user', userRoutes);

module.exports = router;