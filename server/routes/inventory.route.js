import express from 'express';
import validate from 'express-validation';
import inventory from '../beans/inventory';
import inventoryParams from '../params/inventory.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/createInventory')
  /** check user permissions */
  .all(checkPermissions('create_inventory'))
  /** POST inventory/createInventory - create inventory by permitted users */
  .post(validate(inventoryParams.createInventory), inventory.createInventory);

router
  .route('/listInventory')
  /** check user permissions */
  .all(checkPermissions('list_inventory'))
  /** GET inventory/listInventories - list inventory by permitted users */
  .get(validate(inventoryParams.listInventory), inventory.listInventory);

router
  .route('/getInventory/:inventoryId')
  /** check user permissions */
  .all(checkPermissions('get_inventory'))
  /** GET inventory/getInventory - get inventory by id */
  .get(validate(inventoryParams.getInventory), inventory.getInventory);

router
  .route('/deleteInventory')
  /** check user permissions */
  .all(checkPermissions('delete_inventory'))
  /** DELETE inventory/deleteInventory - delete inventory by permitted users */
  .delete(validate(inventoryParams.deleteInventory), inventory.deleteInventory);

router
  .route('/updateInventory')
  /** check user permissions */
  .all(checkPermissions('update_inventory'))
  /** PUT inventory/updateInventory - update inventory by permitted users */
  .put(validate(inventoryParams.updateInventory), inventory.updateInventory);

router
  .route('/getInventoryList')
  /** check user permissions */
  .all(checkPermissions('get_inventory_list'))
  /** GET inventory/listInventories - get list inventory for delivery by permitted users */
  .get(validate(inventoryParams.getInventoryList), inventory.getInventoryList);

router
  .route('/updateInventoryStatus')
  /** check user permissions */
  .all(checkPermissions('update_inventory_status'))
  /** PUT inventory/updateInventoryStatus - update inventory status by permitted users */
  .put(validate(inventoryParams.updateInventoryStatus), inventory.updateInventoryStatus);

module.exports = router;