import express from 'express';
import validate from 'express-validation';
import fee from '../beans/fee';
import feeParams from '../params/fee.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/createFee')
  /** check user permissions */
  .all(checkPermissions('create_fee'))
  /** POST fee/createFee - create fee */
  .post(validate(feeParams.createFee), fee.createFee);

router
  .route('/getFee')
  /** check user permissions */
  .all(checkPermissions('get_fee'))
  /** GET fee/getFee - get fee by id */
  .get(validate(feeParams.getFee), fee.getFee);

router
  .route('/updateFee')
  /** check user permissions */
  .all(checkPermissions('update_fee'))
  /** PUT fee/updateFee - update fee */
  .put(validate(feeParams.updateFee), fee.updateFee);

module.exports = router;