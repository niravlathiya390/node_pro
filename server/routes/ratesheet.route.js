import express from 'express';
import validate from 'express-validation';
import ratesheet from '../beans/ratesheet';
import ratesheetParams from '../params/ratesheet.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/createRatesheet')
  /** check user permissions */
  .all(checkPermissions('create_ratesheet'))
  /** POST ratesheet/createRatesheet - create ratesheet */
  .post(validate(ratesheetParams.createRatesheet), ratesheet.createRatesheet);

router
  .route('/listRatesheets')
  /** check user permissions */
  .all(checkPermissions('list_ratesheets'))
  /** GET ratesheet/listRatesheets - list ratesheets  */
  .get(validate(ratesheetParams.listRatesheets), ratesheet.listRatesheets);

router
  .route('/getRatesheet/:ratesheetId')
  /** check user permissions */
  .all(checkPermissions('get_ratesheet'))
  /** GET ratesheet/getRatesheet - get ratesheet by id */
  .get(validate(ratesheetParams.getRatesheet), ratesheet.getRatesheet);

router
  .route('/updateRatesheet')
  /** check user permissions */
  .all(checkPermissions('update_ratesheet'))
  /** PUT ratesheet/updateRatesheet - update ratesheet */
  .put(validate(ratesheetParams.updateRatesheet), ratesheet.updateRatesheet);

router
  .route('/deleteRatesheet')
  /** check user permissions */
  .all(checkPermissions('delete_ratesheet'))
  /** DELETE ratesheet/deleteRatesheet - delete ratesheet */
  .delete(validate(ratesheetParams.deleteRatesheet), ratesheet.deleteRatesheet);

router
  .route('/updateRatesheetStatus')
  /** check user permissions */
  .all(checkPermissions('update_ratesheet_status'))
  /** PUT ratesheet/updateRatesheetStatus - update ratesheet status */
  .put(validate(ratesheetParams.updateRatesheetStatus), ratesheet.updateRatesheetStatus);

module.exports = router;