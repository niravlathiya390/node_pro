import express from 'express';
import validate from 'express-validation';
import client from '../beans/client';
import clientParams from '../params/client.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/createClient')
  /** check user permissions */
  .all(checkPermissions('create_client'))
  /** POST client/createClient - create client */
  .post(validate(clientParams.createClient), client.createClient);

router
  .route('/listClients')
  /** check user permissions */
  .all(checkPermissions('list_clients'))
  /** GET client/listClients - list clients */
  .get(validate(clientParams.listClients), client.listClients);

router
  .route('/getClient/:clientId')
  /** check user permissions */
  .all(checkPermissions('get_client'))
  /** GET client/getClient - get client by id */
  .get(validate(clientParams.getClient), client.getClient);

router
  .route('/updateClient')
  /** check user permissions */
  .all(checkPermissions('update_client'))
  /** PUT client/updateClient - update client */
  .put(validate(clientParams.updateClient), client.updateClient);

router
  .route('/deleteClient')
  /** check user permissions */
  .all(checkPermissions('delete_client'))
  /** DELETE client/deleteClient - delete client */
  .delete(validate(clientParams.deleteClient), client.deleteClient);

module.exports = router;