import express from 'express';
import validate from 'express-validation';
import vendor from '../beans/vendor';
import vendorParams from '../params/vendor.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/createVendor')
  /** check user permissions */
  .all(checkPermissions('create_vendor'))
  /** POST vendor/createVendor - create vendor by permitted users */
  .post(validate(vendorParams.createVendor), vendor.createVendor);

router
  .route('/listVendors')
  /** check user permissions */
  .all(checkPermissions('list_vendors'))
  /** GET vendor/listVendors - list vendors by permitted users */
  .get(validate(vendorParams.listVendors), vendor.listVendors);

router
  .route('/getVendor/:vendorId')
  /** check user permissions */
  .all(checkPermissions('get_vendor'))
  /** GET vendor/getVendor - get vendor by id */
  .get(validate(vendorParams.getVendor), vendor.getVendor);

router
  .route('/deleteVendor/:vendorId')
  /** check user permissions */
  .all(checkPermissions('delete_vendor'))
  /** DELETE vendor/deleteVendor - delete vendors by permitted users */
  .delete(validate(vendorParams.deleteVendor), vendor.deleteVendor);

router
  .route('/updateVendor')
  /** check user permissions */
  .all(checkPermissions('update_vendor'))
  /** PUT vendor/updateVendor - update vendors by permitted users */
  .put(validate(vendorParams.updateVendor), vendor.updateVendor);

router
  .route('/getVendorList')
  /** check user permissions */
  .all(checkPermissions('get_vendor_list'))
  /** PUT vendor/getVendorList - get list vendors by permitted users */
  .get(vendor.getVendorList);

module.exports = router;