import express from 'express';
import validate from 'express-validation';
import delivery from '../beans/delivery';
import deliveryParams from '../params/delivery.params';
import { checkPermissions } from '../helpers/middleware';

const router = express.Router();

router
  .route('/createDelivery')
  /** check user permissions */
  .all(checkPermissions('create_delivery'))
  /** POST delivery/createDelivery - create delivery */
  .post(validate(deliveryParams.createDelivery), delivery.createDelivery);

router
  .route('/listDeliveries')
  /** check user permissions */
  .all(checkPermissions('list_deliveries'))
  /** GET delivery/listDeliveries - list delivery by permitted users */
  .get(validate(deliveryParams.listDeliveries), delivery.listDeliveries);

router
  .route('/getDelivery/:deliveryId')
  /** check user permissions */
  .all(checkPermissions('get_delivery'))
  /** GET delivery/getDelivery - get delivery by id */
  .get(validate(deliveryParams.getDelivery), delivery.getDelivery);

router
  .route('/updateDelivery')
  /** check user permissions */
  .all(checkPermissions('update_delivery'))
  /** PUT delivery/updateDelivery - update delivery by permitted users */
  .put(validate(deliveryParams.updateDelivery), delivery.updateDelivery);

router
  .route('/deleteDelivery')
  /** check user permissions */
  .all(checkPermissions('delete_delivery'))
  /** DELETE delivery/deleteDelivery - delete delivery by permitted users */
  .delete(validate(deliveryParams.deleteDelivery), delivery.deleteDelivery);

module.exports = router;