import { mailchimp_email, mailchimp_api_key} from '../../bin/www';
const mailchimp = require('@mailchimp/mailchimp_transactional')(mailchimp_api_key);

/**
 * 
 * @param {string} name - user name
 * @param {string} toEmail - user email
 * @param {string} password - password
 * @returns {Promise}
 */
const sendPasswordMail = async (name, toEmail, password) => {
  const msg = {
    message: {
    from_email: mailchimp_email,
    subject: "User's Password",
    to: [{ email: toEmail, type: "to" }],
    html: `<p>Hi ${name} </p>
      <p>Your password is ${password}</p> 
      <p>You can use this password at time of login.</p>`
    }
  }
  await sendMail(msg);
}

/**
 * 
 * @param {string} msg - message template
 */
async function sendMail(msg) {
  await mailchimp.messages.send(msg).then(() => {
    console.log('Email sent')
  })
  .catch((error) => { console.error(error)})
}

//send forget password mail
const forgetPasswordMail = async (name, link, toEmail) => {
  const msg = {
    message: {
    from_email: mailchimp_email,
    subject: "WSI Reset password",
    to: [{ email: toEmail, type: "to" }],
    html: `<p>Hi ${name} </p>
    <p>Please click on the following <a href=${link}>link</a> to reset your password.</p>
    <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>`,
    }
  }
  await sendMail(msg);
}

module.exports = {
  sendPasswordMail,
  forgetPasswordMail
};