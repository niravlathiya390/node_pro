//user types
const userTypes = {
  designer: 'designer',
  employee: 'employee',
};

//user prefered contact
const userPreferdContact = {
  email: 'Email',
  fax: 'Fax',
};

//incident damaged
const incidentDamaged = {
  damageInTransit: 'Damage in Transit',
  damageInWarehouse: 'Damage in Warehouse',
};

//incident status
const incidentStatus = {
  pendingAssigned: 'Pending Assignment',
  pendingEstimate: 'Pending Estimate',
  estimated: 'Estimated',
};

//estimation status
const estimationStatus = {
  pending: 'Pending',
  approved: 'Approved',
  decline: 'Declined'
}

//ratesheeet size
const ratesheetSize = {
  xs: 'XS',
  sm: 'SM',
  md: 'MD',
  lg: 'LG',
  xl: 'XL',
  vault: 'Vault',
  carton: 'Carton',
  cost: 'Cost',
};

//report generate
const reportFilter = {
  missingLocation: 'Missing Location',
  declaredValue: 'Declared Value',
  receivingLog: 'Receiving Log',
  designerList: 'Designer List',
  ultraServiceClients: 'Ultra Service Clients',
  dvpClients: 'DVP Clients',
  inventorySnapshot: 'Inventory Snapshot',
  nonBillableDeliveries: 'Non bilable deliveries',
};

module.exports = {
  userTypes,
  userPreferdContact,
  incidentDamaged,
  incidentStatus,
  ratesheetSize,
  reportFilter,
  estimationStatus
};