import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import invoiceCtrl from '../controllers/invoice.controller';
import invoice from '../models/invoice.model';
import moment from 'moment';
const Excel = require('exceljs');
const ObjectId = require('mongoose').Types.ObjectId;

/**
 * list invoices by admin
 */
async function listInvoices(req, res, next){
  try {
    let { searchString, designer, startDate, endDate, page_size = 10, page_number = 1 } = req.query;
    if(startDate) startDate = moment(startDate).startOf('day');
    if(endDate) endDate = moment(endDate).endOf('day');

    let filter = { };

    if(designer) filter = { designer: ObjectId(designer) }
    if(startDate && endDate) filter = { ...filter, invoiceDate: { $gte: new Date(startDate), $lte: new Date(endDate)} }

    let query = [
      { $match: filter },
      { $project: { ID: 1, designer: 1, client:1, invoiceDate: 1, deliveryFee: 1, DVP_Fee: 1, pickUp: 1, method: 1, createdAt: 1 }},
      { $lookup: { from: 'client', localField: 'client', foreignField: '_id', as: 'clients'}},
      { $lookup: { from: 'user', localField: 'designer', foreignField: '_id', as: 'designers'}},
      { $project: { ID: 1, designer: { $first: '$designers.name'}, client: { $first: '$clients.client'}, invoiceDate: 1, deliveryFee: 1, DVP_Fee: 1, 
      pickUp: 1, method: 1, createdAt: 1 }}
    ];

    let searchQuery = {
      $match: {
        $or: [
          { ID: { $regex: searchString, $options: 'i' } },
          { client: { $regex: searchString, $options: 'i' } },
        ],
      },
    };

    if(searchString && searchString.length > 0) query = [ ...query, searchQuery ];
    let data = await invoice.aggregate([
      ...query,
      {
        $count: 'totalRecord',
      },
    ]);

    const totalRecords = data.length > 0 ? data[0].totalRecord : 0;
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    query = [
      ...query,
      { $sort: { createdAt: -1 } },
      { $skip: (page_number - 1) * page_size },
      { $limit: page_size },
    ];

    let invoices = await invoice.aggregate(query);

    next({ invoices, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get invoice
 */
async function getInvoice(req, res, next) {
  try {
    let filter = { _id: req.params.invoiceId };

    let invoice = await invoiceCtrl.getOne(filter);
    if(!invoice) return next(new APIError(ErrMessages.invalidInvoice, httpStatus.BAD_REQUEST, true));

    next(invoice);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete invoice by admin
 */
async function deleteInvoice(req, res, next){
  try {
    let filter = { _id: { $in: req.body.invoices } };

    await invoiceCtrl.deleteMany(filter);
    next(SuccessMessages.invoiceDeleted);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * export invoices
 */
async function exportInvoices(req, res, next) {
  try {
    let filter = { _id: { $in: req.body.invoices } };
    let select = 'ID designer client invoiceDate deliveryFee DVP_Fee pickUp method';
    let populate = [
      {
        path: 'designer',
        select: 'name'
      },
      {
        path: 'client',
        select: 'client'
      }
    ];

    let invoices = await invoiceCtrl.list(filter, select, populate);

    // export data in to excel
    let workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet('invoices');

    worksheet.getRow(1).values = ['Index', 'ID', 'Designer', 'Client', 'Invoice Date', 'DeliveryFee', 'DVP Fee', 'Pickup', 'Method'];
    worksheet.getRow(1).font = { bold: true, size: 12 };
    worksheet.getRow(1).alignment = { horizontal: 'center' };

    worksheet.columns = [
      { key: 'index', width: 12 },
      { key: 'ID', width: 12 },
      { key: 'designer', width: 25 },
      { key: 'client', width: 25 },
      { key: 'invoiceDate', width: 25 },
      { key: 'deliveryFee', width: 25 },
      { key: 'DVP_Fee', width: 25 },
      { key: 'pickUp', width: 25 },
      { key: 'method', width: 25 },
    ];

    for (let [index, invoice] of invoices.entries()) {
      let rowData = {
        index: index + 1,
        ID: invoice.ID,
        designer: invoice.designer.name,
        client: invoice.client.client,
        invoiceDate: moment(invoice.invoiceDate).format('DD MMM,YYYY'),
        deliveryFee: invoice.deliveryFee,
        DVP_Fee: invoice.DVP_Fee,
        pickUp: 
          invoice.pickUp === true
          ? 'Yes'
          : 'No',
        method: invoice.method
      };

      worksheet.addRow(rowData);
      worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
    }

    let fileName = 'Invoices.csv';

    res.setHeader('Access-Control-Expose-Headers', "Content-Disposition");
    res.setHeader('Content-Type','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);

    await workbook.csv.write(res).then(() => res.end());
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  listInvoices,
  getInvoice,
  deleteInvoice,
  exportInvoices,
}