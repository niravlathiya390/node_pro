import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { makeUniqueId } from '../helpers/utils';
import deliveryCtrl from '../controllers/delivery.controller';
import feeCtrl from '../controllers/fee.controller';
import invoiceCtrl from '../controllers/invoice.controller'
import delivery from '../models/delivery.model';
const ObjectId = require('mongoose').Types.ObjectId;

/**
 * create delivery by admin
 */
async function createDelivery(req, res, next) {
  try {
    let { designer, client, invoicedOn, total, clientPickUp, options } = req.body;
    let delivery_id = makeUniqueId(6);

    let feeData = await feeCtrl.getOne({}, 'dailyRate', { createdAt: -1 });

    let invoice_id = makeUniqueId(6);
    let invoiceBody = {
      designer,
      client,
      deliveryFee: total,
      invoiceDate: invoicedOn,
      pickUp: clientPickUp,
      DVP_Fee: feeData && feeData.dailyRate ? feeData.dailyRate : 0,
      ID: invoice_id,
      method:
        options.cod === true
          ? 'cod'
          : options.billed === true
          ? 'billed'
          : 'prepay',
    };
    let invoiceData = await invoiceCtrl.create({ ...invoiceBody });

    await deliveryCtrl.create({ ...req.body, ID: delivery_id, invoiceId: invoiceData._id });

    next(SuccessMessages.deliveryCreated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/** 
 * list deliveries by admin
 */
async function listDeliveries(req, res, next){
  try {
    let { searchString, designer, page_size = 10, page_number = 1 } = req.query;

    let filter = {};

    if(designer) filter = { designer: ObjectId(designer) }

    let query = [
      { $match: filter },
      { $project: { ID: 1, designer: 1, client:1, total: 1, invoicedOn: 1, deliveredOn: 1, createdAt: 1 }},
      { $lookup: { from: 'client', localField: 'client', foreignField: '_id', as: 'clients'}},
      { $lookup: { from: 'user', localField: 'designer', foreignField: '_id', as: 'designers'}},
      { $project: { ID: 1, designer: { $first: '$designers.name'}, client: { $first: '$clients.client'}, total: 1, invoicedOn: 1, deliveredOn: 1, 
      createdAt: 1 }}
    ];

    let searchQuery = {
      $match: {
        $or: [
          { ID: { $regex: searchString, $options: 'i' } },
          { client: { $regex: searchString, $options: 'i' } },
        ],
      },
    };

    if(searchString && searchString.length > 0) query = [ ...query, searchQuery ];
    let data = await delivery.aggregate([
      ...query,
      {
        $count: 'totalRecord',
      },
    ]);

    const totalRecords = data.length > 0 ? data[0].totalRecord : 0;
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    query = [
      ...query,
      { $sort: { createdAt: -1 } },
      { $skip: (page_number - 1) * page_size },
      { $limit: page_size },
    ];

    let deliveries = await delivery.aggregate(query);

    next({ deliveries, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get delivery
 */
async function getDelivery(req, res, next) {
  try {
    let filter = { _id: req.params.deliveryId };

    let delivery = await deliveryCtrl.getOne(filter);
    if(!delivery) return next(new APIError(ErrMessages.invalidDelivery, httpStatus.BAD_REQUEST, true));

    next(delivery);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update delivery by admin
 */
async function updateDelivery(req, res, next) {
  try {
    let { designer, client, item, deliveredOn, doNotBill, clientPickUp, notes, total, invoicedOn, closedOn, options, designerNotes } = req.body;
    let _id = req.query.deliveryId;

    let delivery = await deliveryCtrl.getOne({ _id }, 'invoiceId');
    if(!delivery) return next(new APIError(ErrMessages.invalidDelivery, httpStatus.BAD_REQUEST, true));

    let updateObj = {}, updateInvoice = {};
    if(designer) {
      updateObj.designer = designer;
      updateInvoice.designer = designer;
    }
    if(client) {
      updateObj.client = client;
      updateInvoice.client = client;
    }
    if(item) updateObj.item = item;
    if(deliveredOn) updateObj.deliveredOn = deliveredOn;
    if(doNotBill === true || doNotBill === false) updateObj.doNotBill = doNotBill;
    if(clientPickUp === true || clientPickUp === false) {
      updateObj.clientPickUp = clientPickUp;
      updateInvoice.pickUp = clientPickUp;
    }
    if(notes === true || notes === false) updateObj.notes = notes;
    if(total) {
      updateObj.total = total;
      updateInvoice.deliveryFee = total;
    }
    if(invoicedOn) {
      updateObj.invoicedOn = invoicedOn;
      updateInvoice.invoiceDate = invoicedOn;
    }
    if(closedOn) updateObj.closedOn = closedOn;
    if(options) {
      updateObj.options = options;
      updateInvoice.method =
        options.cod === true
          ? "cod"
          : options.billed === true
          ? "billed"
          : "prepay";
    }
    if(designerNotes === true || designerNotes === false) updateObj.designerNotes = designerNotes;

    await deliveryCtrl.update({ _id }, updateObj);
    await invoiceCtrl.update({ _id: delivery.invoiceId }, updateInvoice);
    next(SuccessMessages.deliveryUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete delivery by admin
 */
async function deleteDelivery(req, res, next) {
  try {
    let filter = { _id: req.query.deliveryId };

    let delivery = await deliveryCtrl.getOne(filter);
    if(!delivery) return next(new APIError(ErrMessages.invalidDelivery, httpStatus.BAD_REQUEST, true));

    await deliveryCtrl.deleteOne(filter);
    next(SuccessMessages.deliveryDeleted);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  createDelivery,
  listDeliveries,
  getDelivery,
  updateDelivery,
  deleteDelivery
}