import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import S3UploadService from '../services/file.service';
import incidentCtrl from '../controllers/incident.controller';
import inventoryCtrl from '../controllers/inventory.controller';
import userCtrl from '../controllers/user.controller';
import noteCtrl from '../controllers/notes.controller';
import incident from '../models/incident.model';
const ObjectId = require('mongoose').Types.ObjectId;
import { incidentStatus, estimationStatus } from '../helpers/enum';
import moment from 'moment';

async function addIncident(req, res, next) {
   try {
    const { barCodeId, barCodeItemId, wsiNumber, description, itemPicture, damaged, notes } = req.body;

    const barcodeExist = await inventoryCtrl.getOne({ barcode: barCodeId });
    if(!barcodeExist) return next(new APIError(ErrMessages.invalidBarcode, httpStatus.BAD_REQUEST, true));

    const isUserExist = await userCtrl.getOne({ _id: req.user._id });
    if(!isUserExist) return next(new APIError(ErrMessages.invalidUser, httpStatus.BAD_REQUEST, true));

    const noteObj = await noteCtrl.create({ note: notes });

    const inciData = await incidentCtrl.create({ 
      barCodeId,
      barCodeItemId,
      wsiNumber,
      description,
      itemPicture,
      damaged,
      employee: req.user._id,
      notes: [ noteObj._id ],
      designer: barcodeExist.designer
    });
    next(SuccessMessages.incidentAdded);
   } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
   }
}

/**
 * list incidents --> for web
 */
async function listIncidents(req, res, next) {
  try {
    let { searchString, employeeFilter, page_size = 10, page_number = 1 } = req.query;
    let filter = {};
    let populate = [
      { 
        path: 'employee', 
        select: 'name'
      }
    ];

    if(req.user.role === 'designer') filter = { designer: ObjectId(req.user._id)};
    if(req.user.role === 'vendor') filter = { vendor: ObjectId(req.user._id)};

    if(employeeFilter && employeeFilter.length > 0) filter = { ...filter, employee: ObjectId(employeeFilter) };
    let searchFilter = {
      $or: [
        { barCodeId: { $regex: searchString, $options: 'i' }},
      ]
    };

    if(searchString && searchString.length > 0) filter = { ...filter, ...searchFilter };
    const totalRecords = (await incident.find(filter).countDocuments());
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    let incidents = await incidentCtrl.list(filter, 'employee barCodeId barCodeItemId itemPicture status estimation.estimationStatus', page_size, page_number, populate, { createdAt: -1 });

    next({ incidents, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get incident by id
 */
async function getIncident(req, res, next){
  try {
    let filter = { _id: req.params.incidentId };
    let populate = [
      {
        path: 'employee',
        select: 'name'
      },
      {
        path: 'notes',
        select: 'note createdAt file'
      },
      {
        path: 'vendor',
        select: 'name phoneNumber address email'
      },
      {
        path: 'designer',
        select: 'name'
      }
    ]

    let incident = await incidentCtrl.getOne(filter, '', populate);
    if(!incident) return next(new APIError(ErrMessages.invalidIncident, httpStatus.BAD_REQUEST, true));

    const body = { status: 200, message: 'Success', data: incident };
    res.status(body.status).json(body);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update note
 */
async function updateIncident(req, res, next){
  try {
    const { barCodeId, barCodeItemId, wsiNumber, description, itemPicture, damaged, employee } = req.body;
    const _id = req.query.incidentId;

    let incidentExist = await incidentCtrl.getOne({ _id });
    if(!incidentExist) return next(new APIError(ErrMessages.invalidIncident, httpStatus.BAD_REQUEST, true));

    let updateObj = {};
    if(barCodeId) updateObj.barCodeId = barCodeId;
    if(barCodeItemId) updateObj.barCodeItemId = barCodeItemId;
    if(wsiNumber) updateObj.wsiNumber = wsiNumber;
    if(description) updateObj.description = description;
    if(itemPicture) updateObj.itemPicture = itemPicture;
    if(damaged) updateObj.damaged = damaged;
    if(employee) updateObj.employee = employee;

    await incidentCtrl.update({ _id }, updateObj);
    next(SuccessMessages.incidentUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete incident
 */
async function deleteIncident(req, res, next){
  try {
    let filter = { _id: req.query.incidentId };

    let incident = await incidentCtrl.getOne(filter);
    if(!incident) return next(new APIError(ErrMessages.invalidIncident, httpStatus.BAD_REQUEST, true));

    await incidentCtrl.deleteOne(filter);
    if(incident.itemPicture) await S3UploadService.deleteFile(incident.itemPicture);
    next(SuccessMessages.incidentDeleted);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * create estimation
 */
async function createEstimation(req, res, next) {
  try {
    let filter = { _id: req.query.incidentId };
    let estimation = { ...req.body, estimationDate: moment() };

    await incidentCtrl.update(filter, { $set: { estimation: estimation, status: incidentStatus.estimated } });

    next(SuccessMessages.estimationAdded);
  } catch (err) {
    console
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update vendor details in incident
 */
async function assignToVendor(req, res, next) {
  try {
    let { incidentId, vendorId } = req.query;

    let incidentExist = await incidentCtrl.getOne({ _id: incidentId });
    if(!incidentExist) return next(new APIError(ErrMessages.invalidIncident, httpStatus.BAD_REQUEST, true));

    await incidentCtrl.update({ _id: incidentId }, { $set: { vendor: vendorId, status: incidentStatus.pendingEstimate }});

    next(SuccessMessages.vendorAssign);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update estimate status
 */
async function updateEstimationStatus(req, res, next) {
  try {
    let { incidentId, estimationStatus } = req.query;

    let incidentExist = await incidentCtrl.getOne({ _id: incidentId });
    if(!incidentExist) return next(new APIError(ErrMessages.invalidIncident, httpStatus.BAD_REQUEST, true));

    await incidentCtrl.update({ _id: incidentId }, { $set: { 'estimation.estimationStatus': estimationStatus }});

    next(SuccessMessages.estimationUpdate)
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  addIncident,
  listIncidents,
  getIncident,
  updateIncident,
  deleteIncident,
  createEstimation,
  assignToVendor,
  updateEstimationStatus
}