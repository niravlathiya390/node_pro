import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import bcrypt from 'bcrypt';
import { jwtSecret, expiresIn, apiLink, salt } from '../../bin/www';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { forgetPasswordMail } from '../services/mandrill';
import userCtrl from '../controllers/user.controller';
import resetPassword from '../models/resetPassword.model';
import user from '../models/user.model';

/**
 * login
 */
async function login(req, res, next) {
  try {
    let { email, password } = req.body;

    let listUser = await userCtrl.getOne({ email });
    if (!listUser) return next(new APIError(ErrMessages.invalidEmail, httpStatus.BAD_REQUEST, true));

    if(listUser.role === 'employee') return next(new APIError(ErrMessages.unauthorizedUserLogin, httpStatus.BAD_REQUEST, true));

    const validPass = await listUser.matchPassword(password);
    if (!validPass) return next(new APIError(ErrMessages.incorrectPass, httpStatus.BAD_REQUEST, true));

    const token = jwt.sign({ userId: listUser._id, email, role: listUser.role }, jwtSecret, { expiresIn });
    await userCtrl.update({ _id: listUser._id }, { $push: { activeSessions: { $each: [token], $slice: -5 } } });

    next({ name: listUser.name.split(' ')[0], profilePicture: listUser.profilePicture, token, _id: listUser._id, role: listUser.role });
  } catch (err) {
    console.log(err);
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * app login
 */
async function appLogin(req, res, next) {
  try {
    let { email, password } = req.body;

    let listUser = await userCtrl.getOne({ email });
    if (!listUser) return next(new APIError(ErrMessages.invalidEmail, httpStatus.BAD_REQUEST, true));

    if(listUser.role === 'admin' || listUser.role === 'designer' || listUser.role === 'vendor') return next(new APIError(ErrMessages.unauthorizedUserLogin, httpStatus.BAD_REQUEST, true));

    const validPass = await listUser.matchPassword(password);
    if (!validPass) return next(new APIError(ErrMessages.incorrectPass, httpStatus.BAD_REQUEST, true));

    const token = jwt.sign({ userId: listUser._id, email, role: listUser.role }, jwtSecret, { expiresIn });
    await userCtrl.update({ _id: listUser._id }, { $push: { activeSessions: { $each: [token], $slice: -5 } } });

    next({ name: listUser.name.split(' ')[0], profilePicture: listUser.profilePicture, token, _id: listUser._id });
  } catch (err) {
    console.log(err);
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * authorize middleware to check if user is logged in or not
 */
async function authorize(req, res, next) {
  try {
    let token;
    let error;
    if (req.headers.authorization) {
      if (
        typeof req.headers.authorization !== 'string' ||
        req.headers.authorization.indexOf('Bearer ') === -1
      ) {
        error = ErrMessages.badAuth;
      } else {
        token = req.headers.authorization.split(' ')[1];
      }
    } else {
      error = ErrMessages.tokenNot;
    }

    if(!token && error) {
      return next(new APIError(error, httpStatus.UNAUTHORIZED, true));
    }

    return jwt.verify(token, jwtSecret, async (err, decoded) => {
      if (err || !decoded || !decoded.userId) {
        return next(new APIError(ErrMessages.badToken, httpStatus.UNAUTHORIZED, true));
      }
      const userObj = await userCtrl.getOne({ _id: decoded.userId });
      if (!userObj)
        return next(new APIError(ErrMessages.userNotFound, httpStatus.NOT_FOUND, true));
      if (!userObj.activeSessions.includes(token))
        return next(
          new APIError(
            ErrMessages.sessionExpired,
            httpStatus.UNAUTHORIZED,
            true
          )
        );

      req.user = userObj;
      return next();
    });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true, err));
  }
};

/**
 * forget password by user
 */
async function forgotPassword(req, res, next) {
  try {
    let { email } = req.body;
    email = email.toLowerCase();

    const findUser = await userCtrl.getOne({ email });
    if(!findUser) return next(new APIError(ErrMessages.invalidEmail, httpStatus.BAD_REQUEST, true));

    //delete previously generated tokens for users
    let tokenExist = await resetPassword.findOne({ userId: findUser._id });
    if(tokenExist) await resetPassword.deleteMany({ userId: findUser._id })

    // generate reset token
    let resetToken = crypto.randomBytes(32).toString("hex");
    let tokenHex = await bcrypt.hash(resetToken, salt);

    let tokenDetail = await resetPassword.create({
      userId: findUser._id,
      token: tokenHex
    })

    let url = `${apiLink}/auth/renderForgetPasswordForm?id=${tokenDetail.userId}&token=${tokenDetail.token}`;
    let name = findUser.name.split(' ')[0];

    await forgetPasswordMail(name, url, email);

    next(SuccessMessages.resetEmail);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * rendering forgot password template
 */
async function renderForgetPasswordForm(req, res, next) {
  try {
    let { id, token } = req.query;

    let tokenExist = await resetPassword.findOne({ userId: id, token });
    if(!tokenExist) {
      return res.send('Link is expired! Please try again',);
    } 
    
    res.render("form", { link: `${apiLink}/auth/resetUserPassword?id=${id}&token=${token}` });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * reset password
 */
async function resetUserPassword(req, res, next) {
  try {
    let { email, password } = req.body;
    let { id, token } = req.query;
    email = email.toLowerCase();

    const alreadyExist = await userCtrl.getOne({ email, _id: id });
    if (!alreadyExist) return res.send(ErrMessages.incorrectEmail);

    let tokenExist = await resetPassword.findOne({ userId: id, token });
    if(tokenExist){  
      let passwordHex = await bcrypt.hash(password, salt);
      await user.updateOne({ email }, { $set: { password: passwordHex, activeSessions: [] }});

      await resetPassword.deleteMany({ userId: id });
    } else {
      return res.send('Incorrect email');
    }

    next(SuccessMessages.passwordChanged);
  } catch (err) {
    console.log(err);
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  login,
  appLogin,
  authorize,
  forgotPassword,
  renderForgetPasswordForm,
  resetUserPassword
}