import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import feeCtrl from '../controllers/fee.controller';

/**
 * create fee
 */
async function createFee(req, res, next) {
  try {
    await feeCtrl.create(req.body);
    next(SuccessMessages.feeCreated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get fee by id
 */
async function getFee(req, res, next) {
  try {
    let filter = { _id: req.query.feeId };

    let fee = await feeCtrl.getOne(filter);
    if(!fee) return next(new APIError(ErrMessages.invalidFee, httpStatus.BAD_REQUEST, true));

    next(fee);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update fee
 */
async function updateFee(req, res, next) {
  try {
    const { dailyRate, transitRate, pullFee, pickupFee, trashFee, barketFee, laborRates } = req.body;
    let _id = req.query.feeId;

    let updateObj = {};
    if(dailyRate) updateObj.dailyRate = dailyRate;
    if(transitRate) updateObj.transitRate = transitRate;
    if(pullFee) updateObj.pullFee = pullFee;
    if(pickupFee) updateObj.pickupFee = pickupFee;
    if(trashFee) updateObj.trashFee = trashFee;
    if(barketFee) updateObj.barketFee = barketFee;
    if(laborRates) updateObj.laborRates = laborRates;

    await feeCtrl.update({ _id }, updateObj);

    next(SuccessMessages.feeUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  createFee,
  getFee,
  updateFee,
}