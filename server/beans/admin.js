import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Excel from 'exceljs';
import moment from 'moment';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { correctPhoneNumber } from '../helpers/utils';
import { sendPasswordMail } from '../services/mandrill';
import S3UploadService from '../services/file.service';
import userCtrl from '../controllers/user.controller';
import noteCtrl from '../controllers/notes.controller';
import clientCtrl from '../controllers/client.controller';
import incidentCtrl from '../controllers/incident.controller';
import inventoryCtrl from '../controllers/inventory.controller';
import user from '../models/user.model';
const ObjectId = require('mongoose').Types.ObjectId;
import { reportFilter } from '../helpers/enum';
import inventory from '../models/inventory.model';
import deliveryCtrl from '../controllers/delivery.controller';

/**
 * create new user
 */
async function createUser(req, res, next) {
  try {
    let { type, name, email, shipping, billing, preferedContact, billingOptions, sameAsShipping } = req.body;

    if(shipping) {
      if(shipping.business) shipping.business = await correctPhoneNumber(shipping.business);
      if(shipping.cel) shipping.cel = await correctPhoneNumber(shipping.cel);
      if(shipping.fax) shipping.fax = await correctPhoneNumber(shipping.fax);
      if(shipping.other) shipping.other = await correctPhoneNumber(shipping.other);
    }

    if(billing) {
      if(billing.business) billing.business = await correctPhoneNumber(billing.business);
      if(billing.cel) billing.cel = await correctPhoneNumber(billing.cel);
      if(billing.fax) billing.fax = await correctPhoneNumber(billing.fax);
      if(billing.other) billing.other = await correctPhoneNumber(billing.other);
    }

    if(sameAsShipping === true) {
      req.body = { ...req.body, billing: shipping }
    }

    let alreadyPresent = await userCtrl.getOne({ email, role: { $ne: 'vendor'} });
    if(alreadyPresent) return next(new APIError(ErrMessages.sameUser, httpStatus.BAD_REQUEST, true));

    const usr = await userCtrl.create({ ...req.body, role: type });
    name = name.split(' ')[0];
    await sendPasswordMail(name, email, usr.password);

    next(SuccessMessages.userCreated);
  } catch (err) {
    console.log("err",err);
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list users
 */
async function listUsers(req, res, next) {
  try {
    let { searchString, type, page_size = 10, page_number = 1 } = req.query;

    let select = '_id name email type profilePicture';
    let filter = { role: { $in: [ 'designer', 'employee' ] } };
    if(type && type.length > 0) filter = { ...filter, type: type };
    let searchFilter = {
      $or: [
        { name: { $regex: searchString, $options: 'i' }},
        { email: { $regex: searchString, $options: 'i' }},
        { type: { $regex: searchString, $options: 'i' }}
      ]
    };

    if(searchString && searchString.length > 0) filter = { ...filter, ...searchFilter };
    const totalRecords = (await user.find(filter).countDocuments());
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    let users = await userCtrl.list(filter, select, page_size, page_number, { createdAt: -1 });

    next({ users, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get user by Id
 */
async function getUser(req, res, next) {
  try {
    let filter = { _id: req.params.userId };
    let isUser = await userCtrl.getOne(filter);
    if(!isUser) return next(new APIError(ErrMessages.invalidUser, httpStatus.BAD_REQUEST, true));

    const query = { _id: ObjectId(req.params.userId)};
    let userDetail = await user.aggregate([
      {
        $match: query
      },
      {
        $lookup: {
          from: 'notes',
          localField: 'notes',
          foreignField: '_id',
          as: 'notes'
        }
      },
      {
        $project: {
          'notes._id': 1, 'notes.note': 1, 'notes.createdAt': 1, name : 1, email : 1, type : 1, profilePicture : 1, shipping : 1,
          billing : 1, preferedContact: 1, billingOptions: 1, role : 1, createdAt : 1, updatedAt : 1, type : 1, sameAsShipping: 1
        }
      }
    ])

    next(userDetail);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete user
 */
async function deleteUser(req, res, next) {
  try {
    let filter = { _id: req.query.userId };

    let user = await userCtrl.getOne(filter);
    if(!user) return next(new APIError(ErrMessages.invalidUser, httpStatus.BAD_REQUEST, true));

    await userCtrl.deleteOne(filter);
    next(SuccessMessages.userDeleted);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update user
 */
async function updateUser(req, res, next) {
  try {
    const { type, name, email, shipping, sameAsShipping, billing, preferedContact, billingOptions } = req.body;
    const _id = req.query.userId;

    let userExist = await userCtrl.getOne({ _id });
    if(!userExist) return next(new APIError(ErrMessages.userNotFound, httpStatus.BAD_REQUEST, true));

    let findEmail = await userCtrl.getOne({ _id: { $ne: _id }, email, role: { $ne: 'vendor'} });
    if(findEmail) return next(new APIError(ErrMessages.sameUser, httpStatus.BAD_REQUEST, true));

    let updateObj = {};
    if(type) { 
      updateObj.type = type
      updateObj.role = type
    }
    if(name) updateObj.name = name;
    if(email) updateObj.email = email;
    if(shipping)  { 
      if(shipping.business) shipping.business = await correctPhoneNumber(shipping.business);
      if(shipping.cel) shipping.cel = await correctPhoneNumber(shipping.cel);
      if(shipping.fax) shipping.fax = await correctPhoneNumber(shipping.fax);
      if(shipping.other) shipping.other = await correctPhoneNumber(shipping.other);
      updateObj.shipping = shipping;
    }
    if(sameAsShipping === true || sameAsShipping === false) updateObj.sameAsShipping = sameAsShipping;
    if(billing) {
      if(billing.business) billing.business = await correctPhoneNumber(billing.business);
      if(billing.cel) billing.cel = await correctPhoneNumber(billing.cel);
      if(billing.fax) billing.fax = await correctPhoneNumber(billing.fax);
      if(billing.other) billing.other = await correctPhoneNumber(billing.other);
      updateObj.billing = billing;
    };
    if(preferedContact) updateObj.preferedContact = preferedContact;
    if(billingOptions) updateObj.billingOptions = billingOptions;

    if(sameAsShipping === true) {
      updateObj.billing = shipping
    }

    await userCtrl.update({ _id }, updateObj);
    next(SuccessMessages.userUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function addNote(req, res, next) {
  try {
    const { note, file, userId, incidentId } = req.body;
    let validUser, validIncident;

    if(userId && userId.length > 0) {
      validUser = await userCtrl.getOne({ _id: userId });
      if(!validUser) return next(new APIError(ErrMessages.invalidUser, httpStatus.BAD_REQUEST, true));
    }

    if(incidentId && incidentId.length > 0){
      validIncident = await incidentCtrl.getOne({ _id: incidentId });
      if(!validIncident) return next(new APIError(ErrMessages.invalidIncident, httpStatus.BAD_REQUEST, true));
    }

    const noteObj = await noteCtrl.create(req.body);
    if(validUser) await userCtrl.update({ _id: userId }, { $push: { notes: noteObj._id } });
    if(validIncident) await incidentCtrl.update({ _id: incidentId }, { $push: { notes: noteObj._id } });
    next(noteObj);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function updateNote(req, res, next) {
  try {
    const { note, file } = req.body;
    const filter = { _id: req.query.noteId };

    let noteObj = await noteCtrl.getOne(filter);
    if(!noteObj) return next(new APIError(ErrMessages.invalidNote, httpStatus.BAD_REQUEST, true));

    let updateObj = {};
    if(note) updateObj.note = note;
    if(file) updateObj.file = file;

    await noteCtrl.update(filter, updateObj);
    next(SuccessMessages.noteUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function deleteNote(req, res, next) {
  try {
    const { noteId } = req.query;

    let validUser = await userCtrl.getOne({ _id: req.user._id });
    if(!validUser) return next(new APIError(ErrMessages.invalidUser, httpStatus.BAD_REQUEST, true));

    let noteObj = await noteCtrl.getOne({ _id: noteId });
    if(!noteObj) return next(new APIError(ErrMessages.invalidNote, httpStatus.BAD_REQUEST, true));

    if(noteObj.userId) await userCtrl.update({ _id: noteObj.userId }, { $pull: { notes: noteId }});
    if(noteObj.incidentId) await incidentCtrl.update({ _id: noteObj.incidentId }, { $pull: { notes: noteId }});
    if(noteObj.file) await S3UploadService.deleteFile(noteObj.file);

    await noteCtrl.deleteOne({ _id: noteId });
    next(SuccessMessages.notedeleted);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function uploadFile(req, res, next) {
  try {
    let files = req.body.files;
    files = files.map(e => ({ url: e.path, fileName: e.filename, type: e.type, size: e.size }));
    next(files);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function deleteFile(req, res, next) {
  try {
    await S3UploadService.deleteFile(req.body.url)
    next(SuccessMessages.fileDeleteSuccess);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list clients
 */
async function listClients(req, res, next){
  try {
    let { userId } = req.query;
    let select = '_id ID client primaryAddress.address';
    let filter = { designer: userId };

    const clients = await clientCtrl.list(filter, select);
    next(clients);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list designers
 */
async function listDesigners(req, res, next) {
  try {
    let select = '_id name';

    let designers = await userCtrl.list({ role: 'designer' }, select);
    next(designers);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list employees
 */
async function listEmployees(req, res, next) {
  try {
    let select = '_id name';

    let employees = await userCtrl.list({ role: 'employee' }, select);
    next(employees);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * client list
 */
async function clientList(req, res, next) {
  try {
    let filter = { designer: req.query.designerId };
    let select = '_id client';

    let clients = await clientCtrl.list(filter, select);
    next(clients);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get user --> Setting
 */
async function getUserById(req, res, next) {
  try {
    let select;
    if(['admin', 'vendor'].includes(req.user.role)) select = 'name role email phoneNumber profilePicture';
    if(!['admin', 'vendor'].includes(req.user.role)) select = 'name role email shipping.business profilePicture';

    let user = await userCtrl.getOne({ _id: req.user._id }, select);
    user = user ? user : {};

    next(user);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update user
 */
async function userUpdate(req, res, next) {
  try {
    let { name, email, phoneNumber, profilePicture } = req.body;
    const _id = req.user._id;

    let userExist = await userCtrl.getOne({ _id });
    if(!userExist) return next(new APIError(ErrMessages.userNotFound, httpStatus.BAD_REQUEST, true));

    let findEmail = await userCtrl.getOne({ _id: { $ne: _id }, email });
    if(findEmail) return next(new APIError(ErrMessages.sameUser, httpStatus.BAD_REQUEST, true));

    let updateObj = {};
    if(name) updateObj.name = name;
    if(email) updateObj.email = email;
    if(profilePicture) updateObj.profilePicture = profilePicture;
    if(phoneNumber) phoneNumber = await correctPhoneNumber(phoneNumber);

    if(!['admin', 'vendor'].includes(userExist.role)) {
      await userCtrl.update({ _id }, { ...updateObj,  $set: { 'shipping.business': phoneNumber }});
    }
    if(['admin', 'vendor'].includes(userExist.role)) {
      await userCtrl.update({ _id }, { ...updateObj, phoneNumber: phoneNumber });
    }
    next(SuccessMessages.userUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * generate report
 */
async function generateReport(req, res, next) {
  try {
    let { type, startOn, endOn, designer, client, sortBy } = req.query;
    let report = [], fileName, obj = {};

    let workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet('report');

    //missing location
    if(type === reportFilter.missingLocation) {
      report = await inventoryCtrl.list({ $or: [{ location: { $exists: false } }, { location: { $exists: true, $eq: "" } }] }, 'description ID receivedDate');

      worksheet.getRow(1).values = ['ID', 'Item', 'Received On'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'id', width: 12 },
        { key: 'item', width: 25 },
        { key: 'receivedOn', width: 25 },
      ];

      for (let [index, item] of report.entries()) {
        let rowData = {
          id: item.ID,
          item: item.description,
          receivedOn: moment(item.receivedDate).format('MM/DD/YYYY'),
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'MissingLocation.csv';
    }

    //declared value
    if(type === reportFilter.declaredValue) {
      report = await inventory.aggregate([
        { $project: { client: 1, designer: 1, declaredValue: 1 }},
        { $group: { _id: { client: '$client', designer: '$designer' }, total: { $sum : '$declaredValue' }}},
        { $lookup: { from: 'client', localField: '_id.client', foreignField: '_id', as: 'clients' }},
        { $project: { _id: 0, client: '$_id.client', designer: '$_id.designer', totalAmount: '$total', clientName: { $first: '$clients.client'} }},
        { $group: { _id: { designer: '$designer' }, data: { $push: { clientName: '$clientName', declaredValue: '$totalAmount' }},
            totalDeclaredValue: { $sum: '$totalAmount' }}},
        { $lookup: { from: 'user', localField: '_id.designer', foreignField: '_id', as: 'designers' }},
        { $project: { _id: 0, designer: '$_id.designer', designerName: { $first: '$designers.name'}, clients: '$data', totalDeclaredValue: '$totalDeclaredValue' }}
      ]);

      worksheet.getRow(1).values = ['Designer', 'Clients', 'Total Declared Value'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'designer', width: 20 },
        { key: 'clients', width: 70 },
        { key: 'declaredValue', width: 20 },
      ];

      for (let [index, item] of report.entries()) {
        let obj = {};
        if(item.clients && item.clients.length > 0) {
          for(let i = 0; i < item.clients.length; i++){
            const { clientName, declaredValue } = item.clients[i];
            obj[clientName] = ` $${declaredValue}`;
          };
        }

        let rowData = {
          designer: item.designerName,
          clients: JSON.stringify(obj).replace(/['"/{()}]+/g, ''),
          declaredValue: `$${item.totalDeclaredValue}`,
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'DeclaredValues.csv';
    }

    //receiving log
    if(type === reportFilter.receivingLog) {
      if(startOn && startOn.length > 0) startOn = moment(startOn).startOf('day');
      if(endOn && endOn.length > 0) endOn = moment(endOn).endOf('day');
      let filter = {};

      if(startOn && startOn.length > 0 && endOn && endOn.length > 0) filter = { receivedDate: { $gte: new Date(startOn), $lte: new Date(endOn) } };
      if(designer && designer.length > 0) filter = { ...filter, designer: designer };
      if(client && client.length > 0) filter = { ...filter, client: client };
      if(sortBy && sortBy.length > 0) obj[sortBy] = 1;

      report = await inventoryCtrl.list(filter, 'description ID receivedDate location freightLine', 0, 0, [], obj);

      worksheet.getRow(1).values = ['ID', 'Item', 'Location', 'Freight Line', 'Received On'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'id', width: 12 },
        { key: 'description', width: 12 },
        { key: 'location', width: 20 },
        { key: 'freightLine', width: 20 },
        { key: 'receivedDate', width: 20 },
      ];

      for (let [index, item] of report.entries()) {
        let rowData = {
          id: item.ID,
          description: item.description,
          location: item.location,
          freightLine: item.freightLine,
          receivedDate: moment(item.receivedDate).format('MM/DD/YYYY'),
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'ReceivingLog.csv';
    }

    //designer list
    if (type === reportFilter.designerList) { 
      report = await userCtrl.list({ role: 'designer' }, 'name email shipping');

      worksheet.getRow(1).values = ['Designer', 'Address', 'Business', 'Cell', 'Fax', 'Other', 'Email'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'designer', width: 12 },
        { key: 'address', width: 25 },
        { key: 'business', width: 25 },
        { key: 'cell', width: 25 },
        { key: 'fax', width: 25 },
        { key: 'other', width: 25 },
        { key: 'email', width: 30 },
      ];

      for (let [index, item] of report.entries()) {
        let rowData = {
          designer: item.name,
          address: `${item.shipping.address}, ${item.shipping.city}, ${item.shipping.state} ${item.shipping.zipcode}`,
          business: item.shipping.business,
          cell: item.shipping.cel,
          fax: item.shipping.fax,
          other: item.shipping.other,
          email: item.email,
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'DesignerList.csv';
    }

    //ultra service clients
    if (type === reportFilter.ultraServiceClients) {
      report = await clientCtrl.list({ 'options.ultraService': true }, 'client primaryAddress email' )
      worksheet.getRow(1).values = ['Client', 'Address', 'Business', 'Cell', 'Fax', 'Email'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'client', width: 12 },
        { key: 'address', width: 25 },
        { key: 'business', width: 25 },
        { key: 'cell', width: 25 },
        { key: 'fax', width: 25 },
        { key: 'email', width: 30 },
      ];

      for (let [index, item] of report.entries()) {
        let rowData = {
          client: item.client,
          address: `${item.primaryAddress.address}, ${item.primaryAddress.city}, ${item.primaryAddress.state} ${item.primaryAddress.zipcode}`,
          business: item.primaryAddress.business,
          cell: item.primaryAddress.cel,
          fax: item.primaryAddress.fax,
          email: item.email,
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'UltraServiceClient.csv';
    }

    //DVP clients
    if (type === reportFilter.dvpClients) {
      report = await clientCtrl.list({ acceptDVP: true }, 'client primaryAddress email' )
      worksheet.getRow(1).values = ['Client', 'Address', 'Business', 'Cell', 'Fax', 'Email'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'client', width: 12 },
        { key: 'address', width: 25 },
        { key: 'business', width: 25 },
        { key: 'cell', width: 25 },
        { key: 'fax', width: 25 },
        { key: 'email', width: 30 },
      ];

      for (let [index, item] of report.entries()) {
        let rowData = {
          client: item.client,
          address: `${item.primaryAddress.address}, ${item.primaryAddress.city}, ${item.primaryAddress.state} ${item.primaryAddress.zipcode}`,
          business: item.primaryAddress.business,
          cell: item.primaryAddress.cel,
          fax: item.primaryAddress.fax,
          email: item.email,
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'DVPClients.csv';
    }

    //inventory snapshot
    if(type === reportFilter.inventorySnapshot) {
      let filter = {};
      if(designer && designer.length > 0) filter = { ...filter, designer: designer };
      if(client && client.length > 0) filter = { ...filter, client: client };
      if(sortBy && sortBy.length > 0) obj[sortBy] = 1;

      report = await inventoryCtrl.list(filter, 'description ID receivedDate location freightLine', 0, 0, [], obj);

      worksheet.getRow(1).values = ['ID', 'Item', 'Location', 'Freight Line', 'Received On'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'id', width: 12 },
        { key: 'description', width: 12 },
        { key: 'location', width: 20 },
        { key: 'freightLine', width: 20 },
        { key: 'receivedDate', width: 20 },
      ];

      for (let [index, item] of report.entries()) {
        let rowData = {
          id: item.ID,
          description: item.description,
          location: item.location,
          freightLine: item.freightLine,
          receivedDate: moment(item.receivedDate).format('MM/DD/YYYY'),
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'InventorySnapshot.csv';
    }

    //Non billable deliveries
    if (type === reportFilter.nonBillableDeliveries) {
      if(startOn && startOn.length > 0) startOn = moment(startOn).startOf('day');
      if(endOn && endOn.length > 0) endOn = moment(endOn).endOf('day');
      
      let filter = { doNotBill: true };
      if(startOn && startOn.length > 0 && endOn && endOn.length > 0) filter = { ...filter, deliveredOn: { $gte: new Date(startOn), $lte: new Date(endOn) } };

      let populate = [
        { path: 'client', select: 'client' },
        { path: 'designer', select: 'name' },
        { path: 'item', select: 'description' }
      ]
      report = await deliveryCtrl.list(filter, 'ID designer client item deliveredOn', 0, 0, {}, populate)

      worksheet.getRow(1).values = ['ID', 'Designer', 'Client', 'Item', 'Delivery Date'];
      worksheet.getRow(1).font = { bold: true, size: 12 };
      worksheet.getRow(1).alignment = { horizontal: 'center' };

      worksheet.columns = [
        { key: 'id', width: 12 },
        { key: 'designer', width: 12 },
        { key: 'client', width: 20 },
        { key: 'items', width: 20 },
        { key: 'deliveryDate', width: 20 },
      ];

      for (let [index, item] of report.entries()) {
        let rowData = {
          id: item.ID,
          designer: item.designer.name,
          client: item.client.client,
          items: item.item.description,
          deliveryDate: moment(item.deliveredOn).format('MM/DD/YYYY'),
        };

        worksheet.addRow(rowData);
        worksheet.getRow(index + 2).alignment = { horizontal: 'left' };
      }

      fileName = 'NonBilableDeliveries.csv';
    }

    res.setHeader('Access-Control-Expose-Headers', 'Content-Disposition');
    res.setHeader(
      'Content-Type',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );
    res.setHeader('Content-Disposition', 'attachment; filename=' + fileName);

    await workbook.csv.write(res).then(() => res.end());

  } catch (err) {
    console.log('err', err)
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  createUser,
  listUsers,
  getUser,
  deleteUser,
  updateUser,
  addNote,
  updateNote,
  deleteNote,
  uploadFile,
  deleteFile,
  listClients,
  listDesigners,
  listEmployees,
  clientList,
  getUserById,
  userUpdate,
  generateReport
};