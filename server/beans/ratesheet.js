import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { makeUniqueId } from '../helpers/utils';
import ratesheetCtrl from '../controllers/ratesheet.controller';
import ratesheet from '../models/ratesheet.model';

/**
 * create ratesheet
 */
async function createRatesheet(req, res, next){
  try {
    let ratesheet_id = makeUniqueId(6);
    await ratesheetCtrl.create({ ...req.body, ID: ratesheet_id });
    next(SuccessMessages.ratesheetCreated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list ratesheets
 */
async function listRatesheets(req, res, next){
  try {
    let { searchString, page_size = 10, page_number = 1 } = req.query;

    let filter = {};

    let query = [
      { $match: filter },
      { $project: { ID: 1, designer: 1, client:1, effectiveOn: 1, ultraService: 1, retail: 1, createdAt: 1 }},
      { $lookup: { from: 'client', localField: 'client', foreignField: '_id', as: 'clients'}},
      { $lookup: { from: 'user', localField: 'designer', foreignField: '_id', as: 'designers'}},
      { $project: { ID: 1, designer: { $first: '$designers.name'}, designerProfile: { $first: '$designers.profilePicture'},  client: { $first: '$clients.client'}, effectiveOn: 1, ultraService: 1, retail: 1, createdAt: 1 }}
    ];

    let searchQuery = {
      $match: {
        $or: [
          { ID: { $regex: searchString, $options: 'i' } },
          { client: { $regex: searchString, $options: 'i' } },
          { designer: { $regex: searchString, $options: 'i' } },
        ],
      },
    };

    if(searchString && searchString.length > 0) query = [ ...query, searchQuery ];
    let data = await ratesheet.aggregate([
      ...query,
      {
        $count: 'totalRecord',
      },
    ]);

    const totalRecords = data.length > 0 ? data[0].totalRecord : 0;
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    query = [
      ...query,
      { $sort: { createdAt: -1 } },
      { $skip: (page_number - 1) * page_size },
      { $limit: page_size },
    ];

    let rateSheets = await ratesheet.aggregate(query);

    next({ rateSheets, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get ratesheet by id
 */
async function getRatesheet(req, res, next) {
  try {
    let filter = { _id: req.params.ratesheetId };

    let ratesheet = await ratesheetCtrl.getOne(filter);
    if(!ratesheet) return next(new APIError(ErrMessages.invalidRatesheet, httpStatus.BAD_REQUEST, true));

    next(ratesheet);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update ratesheet
 */
async function updateRatesheet(req, res, next){
  try {
    let { designer, client, effectiveOn, ratesheet} = req.body;
    let _id = req.query.ratesheetId;

    let updatedValue = {};
    if(designer) updatedValue.designer = designer;
    if(client) updatedValue.client = client;
    if(effectiveOn) updatedValue.effectiveOn = effectiveOn;
    if(ratesheet) updatedValue.ratesheet = ratesheet;

    await ratesheetCtrl.update({ _id }, updatedValue);

    next(SuccessMessages.ratesheetUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete ratesheet
 */
async function deleteRatesheet(req, res, next){
  try {
    let filter = { _id: req.query.ratesheetId };

    let ratesheet = await ratesheetCtrl.getOne(filter);
    if(!ratesheet) return next(new APIError(ErrMessages.invalidRatesheet, httpStatus.BAD_REQUEST, true));

    await ratesheetCtrl.deleteOne(filter);
    next(SuccessMessages.ratesheetDeleted);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update ratesheet status
 */
async function updateRatesheetStatus(req, res, next) {
  try {
    let { ultraService, retail } = req.body;
    let _id = req.query.ratesheetId;

    let updatedValue = {};
    if(ultraService === true || ultraService === false) updatedValue.ultraService = ultraService;
    if(retail === true || retail === false) updatedValue.retail = retail;

    await ratesheetCtrl.update({ _id }, updatedValue);

    next(SuccessMessages.ratesheetStatusUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  createRatesheet,
  listRatesheets,
  getRatesheet,
  updateRatesheet,
  deleteRatesheet,
  updateRatesheetStatus
}