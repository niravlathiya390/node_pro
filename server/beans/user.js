import httpStatus from 'http-status';
import bcrypt from 'bcrypt';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { correctPhoneNumber } from '../helpers/utils';
import userCtrl from '../controllers/user.controller';
import incidentCtrl from '../controllers/incident.controller';
import user from '../models/user.model';
const ObjectId = require('mongoose').Types.ObjectId;

/**
 * change password by user
 */
async function changePassword(req, res, next) {
  try {
    const { oldPassword, newPassword } = req.body;

    if(oldPassword === newPassword) return next(new APIError(ErrMessages.invalidNewPassword, httpStatus.BAD_REQUEST, true));

    let listUser = await userCtrl.getOne({ _id: req.user._id });

    const validPass = await listUser.matchPassword(oldPassword);
    if(!validPass) return next(new APIError(ErrMessages.incorrectPassword, httpStatus.BAD_REQUEST, true));
  
    let passwordHex = await bcrypt.hash(newPassword, 12);
    await userCtrl.update({ _id: req.user._id }, { $set: { password: passwordHex, activeSessions: [] }});

    next(SuccessMessages.passwordChanged);
    } catch(err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get user
 */
async function getUser(req, res, next){
  try {
    let select = 'name email shipping.business';
    let User = await user.findOne({ _id: req.user._id }).select(select);
    next(User);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update user
 */
async function updateUser(req, res, next){
  try {
    let { name, email, business } = req.body;
    const filter = { _id: req.user._id };

    let userExist = await userCtrl.getOne(filter);
    if(!userExist) return next(new APIError(ErrMessages.userNotFound, httpStatus.BAD_REQUEST, true));

    let findEmail = await userCtrl.getOne({ _id: { $ne: req.user._id }, email });
    if(findEmail) return next(new APIError(ErrMessages.sameUser, httpStatus.BAD_REQUEST, true));

    let updateObj = {};
    if(name) updateObj.name = name;
    if(email) updateObj.email = email;
    if(business) business = await correctPhoneNumber(business);

    await userCtrl.update(filter, { ...updateObj,  $set: { 'shipping.business': business }});
    next(SuccessMessages.userUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list incidents --> for app
 */
async function listIncidents(req, res, next) {
  try {
    let { searchString } = req.query;
    const userId = req.user._id;
    let filter = { employee: ObjectId(userId) };

    let searchFilter = {
      $or: [
        { barCodeId: { $regex: searchString, $options: 'i' }},
      ]
    };
    if(searchString && searchString.length > 0) filter = { ...filter, ...searchFilter };

    const incidentlist = await incidentCtrl.list(filter);
    next(incidentlist);
  } catch (err) {
   return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));  
  }
}

module.exports = {
  changePassword,
  getUser,
  updateUser,
  listIncidents,
}