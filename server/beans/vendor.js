import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { correctPhoneNumber } from '../helpers/utils';
import vendorCtrl from '../controllers/vendor.controller';
import user from '../models/user.model';
import { roleObj } from '../helpers/roles'

/**
 * create vendor by admin
 */
async function createVendor(req, res, next) {
  try {
    let { email, phoneNumber } = req.body;

    if(phoneNumber) phoneNumber = await correctPhoneNumber(phoneNumber);

    let findEmail = await vendorCtrl.getOne({ email, role: 'vendor' });
    if(findEmail) return next(new APIError(ErrMessages.sameVendor, httpStatus.BAD_REQUEST, true));

    let findPhone = await vendorCtrl.getOne({ phoneNumber });
    if(findPhone) return next(new APIError(ErrMessages.sameVendorUser, httpStatus.BAD_REQUEST, true));

    let body = { ...req.body, phoneNumber: phoneNumber, role: roleObj.vendor }
    await vendorCtrl.create(body);

    next(SuccessMessages.vendorCreated)
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list vendors by admin
 */
async function listVendors(req, res, next) {
  try {
    let { searchString, page_size = 10, page_number = 1 } = req.query;

    let select = '_id name email phoneNumber address';
    let filter = { role: { $eq: roleObj.vendor } };

    let searchFilter = {
      $or: [{ name: { $regex: searchString, $options: 'i' }},
            { phoneNumber: { $regex: searchString, $options: 'i' }},
            { email: { $regex: searchString, $options: 'i' }},
            { address: { $regex: searchString, $options: 'i' }}],
    };

    if (searchString && searchString.length > 0) filter = { ...filter, ...searchFilter };
    const totalRecords = (await user.find(filter).countDocuments());
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    let users = await vendorCtrl.list(filter, select, page_size, page_number, { createdAt: -1 });

    next({ users, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    console.log(err);
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get vendor by Id
 */
async function getVendor(req, res, next) {
  try {
    let filter = { _id: req.params.vendorId };

    let vendor = await vendorCtrl.getOne(filter);
    if(!vendor) return next(new APIError(ErrMessages.invalidVendor, httpStatus.BAD_REQUEST, true));

    next(vendor);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update vendor by admin
 */
async function updateVendor(req, res, next) {
  try {
    let { email, phoneNumber, name, address, password } = req.body;
    let _id = req.query.vendorId;

    let findEmail = await vendorCtrl.getOne({ _id: { $ne: _id }, email, role: 'vendor' });
    if(findEmail) return next(new APIError(ErrMessages.sameVendor, httpStatus.BAD_REQUEST, true));

    let findPhone = await vendorCtrl.getOne({ _id: { $ne: _id }, phoneNumber });
    if(findPhone) return next(new APIError(ErrMessages.sameVendorUser, httpStatus.BAD_REQUEST, true));

    let updatedValue = {};
    if(email) updatedValue.email = email;
    if(phoneNumber) updatedValue.phoneNumber = await correctPhoneNumber(phoneNumber);
    if(name) updatedValue.name = name;
    if(address) updatedValue.address = address;
    if(password) updatedValue.password = password;

    await vendorCtrl.update({ _id }, updatedValue);

    next(SuccessMessages.vendorUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete vendor by admin
 */
async function deleteVendor(req, res, next) {
  try {
    let _id = req.params.vendorId;

    let findVendor = await vendorCtrl.getOne({ _id });
    if(!findVendor) return next(new APIError(ErrMessages.invalidVendor, httpStatus.BAD_REQUEST, true));

    await user.deleteOne({ _id });

    next(SuccessMessages.vendorDeleted)
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get vendor list
 */
async function getVendorList(req, res, next) {
  try {
    let users = await vendorCtrl.list({ role: { $eq: roleObj.vendor }}, 'name', 0, 0);

    next(users)
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  createVendor,
  listVendors,
  getVendor,
  updateVendor,
  deleteVendor,
  getVendorList
}