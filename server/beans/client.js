import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { correctPhoneNumber } from '../helpers/utils';
import { makeUniqueId } from '../helpers/utils';
import clientCtrl from '../controllers/client.controller';
import client from '../models/client.model';

/**
 * create client by admin
 */
async function createClient(req, res, next) {
  try {
    let { email, primaryAddress, secondaryAddress, sameAsPrimaryAddress } = req.body;

    if(primaryAddress.business) primaryAddress.business = await correctPhoneNumber(primaryAddress.business);
    if(primaryAddress.cel) primaryAddress.cel = await correctPhoneNumber(primaryAddress.cel);
    if(primaryAddress.fax) primaryAddress.fax = await correctPhoneNumber(primaryAddress.fax);
    if(primaryAddress.other) primaryAddress.other = await correctPhoneNumber(primaryAddress.other);

    if(secondaryAddress) {
      if(secondaryAddress.business) secondaryAddress.business = await correctPhoneNumber(secondaryAddress.business);
      if(secondaryAddress.cel) secondaryAddress.cel = await correctPhoneNumber(secondaryAddress.cel);
      if(secondaryAddress.fax) secondaryAddress.fax = await correctPhoneNumber(secondaryAddress.fax);
      if(secondaryAddress.other) secondaryAddress.other = await correctPhoneNumber(secondaryAddress.other);
    }

    if(sameAsPrimaryAddress === true) {
      req.body = { ...req.body, secondaryAddress: primaryAddress }
    }

    let findEmail = await clientCtrl.getOne({ email });
    if(findEmail) return next(new APIError(ErrMessages.sameClient, httpStatus.BAD_REQUEST, true));
    let client_id = makeUniqueId(6);

    await clientCtrl.create({ ...req.body, ID: client_id });

    next(SuccessMessages.clientCreated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list clients by admin
 */
async function listClients(req, res, next) {
  try {
    let { searchString, page_size = 10, page_number = 1 } = req.query;

    let select = '_id ID client designer primaryAddress.address secondaryAddress.address';
    let populate = [
      {
        path: 'designer',
        select: 'name'
      }
    ]
    let filter = { };
    let searchFilter = {
      $or: [
        { client: { $regex: searchString, $options: 'i' }},
        { ID: { $regex: searchString, $options: 'i' }}
      ]
    };

    if(searchString && searchString.length > 0) filter = { ...searchFilter };
    const totalRecords = (await client.find(filter).countDocuments());
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    let clients = await clientCtrl.list(filter, select, populate, page_size, page_number, { createdAt: -1 });

    next({ clients, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get client by Id
 */
async function getClient(req, res, next) {
  try {
    let filter = { _id: req.params.clientId };

    let client = await clientCtrl.getOne(filter);
    if(!client) return next(new APIError(ErrMessages.invalidClient, httpStatus.BAD_REQUEST, true));

    next(client);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update client
 */
async function updateClient(req, res, next) {
  try {
    const { client, designer, email, primaryAddress, sameAsPrimaryAddress, secondaryAddress, acceptDVP, options } = req.body;
    const _id = req.query.clientId;

    let clientExist = await clientCtrl.getOne({ _id });
    if(!clientExist) return next(new APIError(ErrMessages.clientNotFound, httpStatus.BAD_REQUEST, true));

    let findEmail = await clientCtrl.getOne({ _id: { $ne: _id }, email });
    if(findEmail) return next(new APIError(ErrMessages.sameClient, httpStatus.BAD_REQUEST, true));

    let updateObj = {};
    if(client) updateObj.client = client;
    if(designer) updateObj.designer = designer;
    if(email) updateObj.email = email;
    if(primaryAddress)  { 
      if(primaryAddress.business) primaryAddress.business = await correctPhoneNumber(primaryAddress.business);
      if(primaryAddress.cel) primaryAddress.cel = await correctPhoneNumber(primaryAddress.cel);
      if(primaryAddress.fax) primaryAddress.fax = await correctPhoneNumber(primaryAddress.fax);
      if(primaryAddress.other) primaryAddress.other = await correctPhoneNumber(primaryAddress.other);
      updateObj.primaryAddress = primaryAddress;
    }
    if(sameAsPrimaryAddress === true || sameAsPrimaryAddress === false) updateObj.sameAsPrimaryAddress = sameAsPrimaryAddress;
    if(secondaryAddress) {
      if(secondaryAddress.business) secondaryAddress.business = await correctPhoneNumber(secondaryAddress.business);
      if(secondaryAddress.cel) secondaryAddress.cel = await correctPhoneNumber(secondaryAddress.cel);
      if(secondaryAddress.fax) secondaryAddress.fax = await correctPhoneNumber(secondaryAddress.fax);
      if(secondaryAddress.other) secondaryAddress.other = await correctPhoneNumber(secondaryAddress.other);
      updateObj.secondaryAddress = secondaryAddress;
    };
    if(acceptDVP === true || acceptDVP === false) updateObj.acceptDVP = acceptDVP;
    if(options) updateObj.options = options;

    if(sameAsPrimaryAddress === true) {
      updateObj.secondaryAddress = primaryAddress
    }

    await clientCtrl.update({ _id }, updateObj);
    next(SuccessMessages.clientUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete client
 */
async function deleteClient(req, res, next) {
  try {
    let filter = { _id: req.query.clientId };

    let client = await clientCtrl.getOne(filter);
    if(!client) return next(new APIError(ErrMessages.invalidClient, httpStatus.BAD_REQUEST, true));

    await clientCtrl.deleteOne(filter);
    next(SuccessMessages.clientDeleted);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  createClient,
  listClients,
  getClient,
  updateClient,
  deleteClient
}