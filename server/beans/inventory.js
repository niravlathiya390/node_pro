import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { ErrMessages, SuccessMessages } from '../helpers/AppMessages';
import { makeUniqueId } from '../helpers/utils';
import clientCtrl from '../controllers/client.controller';
import inventoryCtrl from '../controllers/inventory.controller';
import inventory from '../models/inventory.model';
const ObjectId = require('mongoose').Types.ObjectId;

/**
 * create inventory by admin
 */
async function createInventory(req, res, next) {
  try {
    let { copy, client } = req.body;
    let inventory_id1 = makeUniqueId(6);
    let inventory_id2 = makeUniqueId(6);

    let select = 'client designer';
    let populate = [
      {
        path: 'designer',
        select: 'name'
      }
    ]
    let clientId = await clientCtrl.getOne({ _id: client }, select, populate);

    let designerCode = clientId.designer.name.replace(" ", "").substring(0,3).toUpperCase()
    let clientCode = clientId.client.replace(" ", "").substring(0,5).toUpperCase()

    let barcode1 = `${designerCode}-${clientCode}-${inventory_id1}`;
    let barcode2 = `${designerCode}-${clientCode}-${inventory_id2}`;

    if(copy === true) {
      await inventoryCtrl.create({ ...req.body, ID: inventory_id1, barcode: barcode1 });
    }

    await inventoryCtrl.create({ ...req.body, ID: inventory_id2, barcode: barcode2 });

    next(SuccessMessages.inventoryCreated)
  } catch (err) {
    console.log(err);
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list inventory by admin
 */
async function listInventory(req, res, next) {
  try {
    let { searchString, designer, page_size = 10, page_number = 1 } = req.query;

    let select = '_id ID description designer client receivedDate deliveredDate status barcode';
    let populate = [
      {
        path: 'designer',
        select: 'name'
      },
      {
        path: 'client',
        select: 'client'
      }
    ];
    let filter = { };

    let searchFilter = {
      $or: [
        { ID: { $regex: searchString, $options: 'i' }},
        { description: { $regex: searchString, $options: 'i' }}
      ]};

    if(designer) filter = { designer: ObjectId(designer) }

    if(searchString && searchString.length > 0) filter = { ...filter, ...searchFilter };
    const totalRecords = (await inventory.find(filter).countDocuments());
    const page_count = totalRecords / page_size || 1;

    if(Math.ceil(page_count) < page_number) {
      page_number = 1
    }

    let inventories = await inventoryCtrl.list(filter, select, page_size, page_number, populate, { createdAt: -1 });

    next({ inventories, totalRecords, page_count: Math.ceil(page_count) });
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * get inventory
 */
async function getInventory(req, res, next) {
  try {
    let filter = { _id: req.params.inventoryId };

    let inventory = await inventoryCtrl.getOne(filter);
    if(!inventory) return next(new APIError(ErrMessages.invalidInventory, httpStatus.BAD_REQUEST, true));

    const body = { status: 200, message: 'Success', data: inventory };
    res.status(body.status).json(body);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update inventory by admin
 */
async function updateInventory(req, res, next) {
  try {
    let { client, designer, poReference, description, size, weight, location, freightLine, declaredValue, noOfBarket, receivedDate, deliveredDate } = req.body;
    let _id = req.query.inventoryId;

    let inventory = await inventoryCtrl.getOne({ _id });
    if(!inventory) return next(new APIError(ErrMessages.invalidInventory, httpStatus.BAD_REQUEST, true));

    let barcode = inventory.barcode;
    if(client !== inventory.client.toString() || designer !== inventory.designer.toString()) {
      let select = 'client designer';
      let populate = [
        {
          path: 'designer',
          select: 'name'
        }
      ]
      let clientId = await clientCtrl.getOne({ _id: client }, select, populate);
      if(!clientId) return next(new APIError(ErrMessages.invalidClient, httpStatus.BAD_REQUEST, true));
  
      let designerCode = clientId.designer.name.replace(" ", "").substring(0,3).toUpperCase()
      let clientCode = clientId.client.replace(" ", "").substring(0,5).toUpperCase()
  
      barcode = `${designerCode}-${clientCode}-${inventory.ID}`;
    }  
  
    let updatedValue = {};
    if(client) updatedValue.client = client;
    if(designer) updatedValue.designer = designer;
    if(poReference || poReference === '') updatedValue.poReference = poReference;
    if(description) updatedValue.description = description;
    if(size) updatedValue.size = size;
    if(weight) updatedValue.weight = weight;
    if(location || location === '') updatedValue.location = location;
    if(freightLine || freightLine === '') updatedValue.freightLine = freightLine;
    if(declaredValue || declaredValue === '') updatedValue.declaredValue = declaredValue;
    if(noOfBarket || noOfBarket === '') updatedValue.noOfBarket = noOfBarket;
    if(receivedDate) updatedValue.receivedDate = receivedDate;
    if(deliveredDate) updatedValue.deliveredDate = deliveredDate;
    if(barcode) updatedValue.barcode = barcode;

    await inventoryCtrl.update({ _id }, updatedValue);

    next(SuccessMessages.inventoryUpdated);
  } catch (err) {
    console.log(err);
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * delete inventory by admin
 */
async function deleteInventory(req, res, next) {
  try {
    let _id = req.query.inventoryId;

    let findVendor = await inventoryCtrl.getOne({ _id });
    if(!findVendor) return next(new APIError(ErrMessages.invalidInventory, httpStatus.BAD_REQUEST, true));

    await inventoryCtrl.deleteOne({ _id });

    next(SuccessMessages.inventoryDeleted)
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * list inventory by admin
 */
async function getInventoryList(req, res, next) {
  try {
    let { designer, client } = req.query;

    let listInventory = await inventoryCtrl.list({ designer, client }, 'description', 0, 0, [], { createdAt: -1 });

    next(listInventory);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

/**
 * update inventory status
 */
async function updateInventoryStatus(req, res, next) {
  try {
    let { status } = req.body;
    let _id = req.query.inventoryId;

    let updatedValue = {};
    if(status === true || status === false) updatedValue.status = status;

    await inventoryCtrl.update({ _id }, updatedValue);
    next(SuccessMessages.inventoryStatusUpdated);
  } catch (err) {
    return next(new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

module.exports = {
  createInventory,
  listInventory,
  getInventory,
  updateInventory,
  deleteInventory,
  getInventoryList,
  updateInventoryStatus
}